#include <type_traits>
#include <vector>
#include <array>
#include <string>
#include <iostream>
#include <tuple>
#include <xmmintrin.h>
#include <cstdint>
#include <cassert>
#include <random>

typedef std::vector < uint8_t > bytes;

//TAGS
struct data_tag{};
struct enc_data_tag{};
struct key_tag{};
struct wrapped_key_tag{};

//ALGS
struct belt
{
    void e()
    {

    }
    void d()
    {

    }
    void gen_key()
    {

    }
};
struct bign
{
    void sign()
    {

    }
    void verify()
    {

    }
    void gen_key()
    {

    }
};

//TYPES

struct key{};
template< class Tag>
class data
{
};

template < class Tag >
bool operator==(data<Tag> const &l, data<Tag> const &r)
{
    return true;
}
template < class Tag >
bool operator!=(data<Tag> const &l, data<Tag> const &r)
{
    return !(l == r);
}


template< class Alg, class Tag, class Rep>
class ikey
{
public:
    //ikey()

    void generate()
    {
        
        //Alg::gen_key();
    }
    void generate_from_pwd()
    {
        //Alg::gen_key_from_pwd();
    }

    data<key_tag> export_secret()
    {
        return data<key_tag>();
    }

    void import_secret(data<key_tag> const &secret)
    {

    }

    template< class TaggedData = data<data_tag> >
    data<enc_data_tag> e(TaggedData const & plain)
    {
        //Alg::e();
        return data<enc_data_tag>();
    };
    template< class EncTaggedData = data<enc_data_tag> >
    data<data_tag> d(EncTaggedData const &edata)
    {
        //Alg::d()
        return data<data_tag>();

    };
};

template<class Tag>
data<Tag> read_file(std::string fnm)
{
    return data<Tag>();
}

template<class TagedData>
void write_file(TagedData const& data, std::string fnm)
{

}

void test()
{
    std::string d1_fnm = "d1_fnm.bin";
    std::string ed1_fnm = "ed1_fnm.bin";
    std::string ed2_fnm = "ed2_fnm.bin";

    auto d1 = read_file<data_tag>(d1_fnm);
    auto k1 = ikey<belt, key_tag, bytes >();
    k1.generate();
    auto ed1 = k1.e(d1);
    write_file(ed1, ed1_fnm);
    auto ed2 = read_file<enc_data_tag>(ed1_fnm);
    assert(ed1 == ed2);

    auto d2 = k1.d(ed1);
    write_file(d2, ed2_fnm);
    auto d3 = read_file<data_tag>(ed2_fnm);
    assert(d3 == d2);
    assert(d1 == d2);
}

void test2()
{
    std::string data_k1_fnm = "data_k1_fnm.bin";
    std::string data_k2_fnm = "data_k2_fnm.bin";

    auto k1 = ikey<belt, key_tag, bytes >();
    k1.generate();
    write_file(k1.export_secret(), data_k1_fnm);
    auto data_k2 = read_file<key_tag>(data_k1_fnm);
    //auto k2 = ikey<belt, key_tag, bytes >( data_k2 );
}
