module Concept where

open import Data.String
open import Data.Product
open import Data.Maybe
open import Data.Bool
open import Data.List hiding ([_])
open import Data.Vec hiding ([_])

open import Data.Nat
open import Data.Fin hiding (_+_)

open import Relation.Binary.PropositionalEquality hiding ([_])
open import Relation.Nullary

byte : Set
byte = Fin 256

word : Set
word = Fin (256 * 256)

_[_] : Set → ℕ → Set
A [ n ] = Vec A n

BeltKey : Set
BeltKey = word [ 8 ]

bytes : Set
bytes = List byte

words : Set
words = List word

two-bytes : bytes [ 2 ]
two-bytes = {!!}

fin2s : List (Fin 10)
fin2s = suc (suc (zero {7})) ∷ suc (zero {8}) ∷ zero ∷ []

-- data Data (tag : Set) : Set where



Data : (tag : Set) → Set
Data = {!!}

data Rng : Set where

{-
data Key (tag : Set) (usage : Set) : Set where
  random-key : Rng → Key tag usage
  from-password : (password : String) → Key tag usage
-}

Key : (tag : Set) (usage : Set) → Set
Key = {!!}
random-key : {tag : Set} {usage : Set} → Rng → Key tag usage
random-key = {!!}
from-password : {tag : Set} {usage : Set} → (password : String) → Key tag usage
from-password = {!!}

Secret : Set
Secret = {!!}

Encrypted : Set → Set → Set
Encrypted = {!!}
enc-secret : {tag : Set} {usage : Set} → Key tag usage → Secret → Encrypted (Key tag usage) Secret
enc-secret = {!!}

data Encrypted′ (key : Set) (secret : Set) : Set where
  enc-secret′ : Encrypted′ key secret

-- enc-secret′ : (key : Set) → (secret : Set) → Encrypted key secret
-- enc-secret′ = {!!}

-- dec-secret′ : (key : Set) → Encrypted key secret → (secret : Set)
-- dec-secret′ = {!!}

_||_ : (a : Set) (b : Set) → Set
_||_ = {!!}

Rep : Set
Rep = {!!}

data Encrypted″ (Key : Set) (Secret : Set) : Set₁ where
  enc-secret″ : ∀ {Rep : Set} (ebody : Rep) → Encrypted″ Key Secret

key-wrap : {Key : Set} {Secret : Set} {Header : Set} 
  (key : Key) (secret : Secret) (header : Header)
  → Encrypted″ Key (Secret || Header)
key-wrap = {!!}

key-unwrap : {Key : Set} {Secret : Set} {Header : Set} 
  (key : Key) → Encrypted″ Key (Secret || Header)
  → Secret × Header
key-unwrap = {!!}

=-Header : {Header : Set} → Header → Header → Bool
=-Header = {!!}

key-unwrap′ : {Key : Set} {Secret : Set} {Header : Set} 
  (key : Key) (header : Header) → Encrypted″ Key (Secret || Header)
  → Maybe Secret
key-unwrap′ key header enc with key-unwrap key enc 
key-unwrap′ key header enc | dec-secret , dec-header with =-Header header dec-header
key-unwrap′ key header enc | dec-secret , dec-header | true = just dec-secret
key-unwrap′ key header enc | dec-secret , dec-header | false = nothing

abstract
  SignRep : Set
  SignRep = {!!}


data Signed (Pri : Set) (HashValue : Set) : Set₁ where
  signed : {Rep : Set}  (sgn : Rep) → Signed Pri HashValue

sign : {Pri : Set} {HashValue : Set} 
  (pri : Pri) (hash-value : HashValue)
  → Signed Pri HashValue
sign pri hash-value = signed {Rep = SignRep} {!!}

sign-rep : {Pri : Set} {HashValue : Set} 
  (pri : Pri) (hash-value : HashValue)
  → (Rep : Set) → Signed Pri HashValue
sign-rep pri hash-value Rep = signed {Rep = Rep} {!!}

verify : {Pri : Set} {HashValue : Set} {Pub : Set → Set}
  (pub : Pub Pri) (hash-value : HashValue)
  → Signed Pri HashValue
  → Bool
verify = {!!}

data DH (Pri : Set) (Pub : Set) : Set where
  

dh : {Pri : Set} {Pub : Set → Set}
  (d : Pri) (Q : Pub Pri)
  → DH Pri (Pub Pri)
dh = {!!}

{-
+ : ℕ → ℕ → ℕ
+ a b = ?
0, 1 ⊂ ℕ ⊂ Set = Set₀ = Set 0 ⊂ Set₁ = Set 1 ⊂ ⋯
+ ⊂ ℕ → ℕ → ℕ ⊂ Set
-}

x+0≡x : ∀ {x} → x + 0 ≡ x
x+0≡x {zero} = refl
x+0≡x {suc x} = cong suc x+0≡x

+-comm : ∀ x y → x + y ≡ y + x
+-comm zero zero = refl
+-comm (suc x) zero = cong suc x+0≡x

+-comm x (suc y) = {!!} -- cong suc (+-comm zero y) -- (sym x+0≡x)
-- +-comm (suc x) (suc y) = cong suc {!!}

0≡1 : ¬ (0 ≡ 1)
0≡1 = λ ()


