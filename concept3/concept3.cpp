// cry_test2.cpp : Defines the entry point for the console application.
//
#include <type_traits>
#include <vector>
#include <array>
#include <string>
#include <iostream>
#include <tuple>
#include <xmmintrin.h>
#include <cstdint>
#include <cassert>
#include <random>
//#include <boost/>

namespace impl {

    /*
    - block-level functions (operate upon single block):
    - typedefs provide block_t & keyx_t (keyx_t - is expanded key, actually used by algorithms);
    - typedefs also provide iv_t, tag_t (authentication tag), key_t<L>, hash_val_t;
    - basic belt functions are block encryption e(k,x) and block decryption d(k,x);
    - hash double-block update function h(ctx,x[2]);
    - authentication functions: omac(s,x) & gmac(s,x);
    - blocks-level functions (operate upon complete chain of blocks):
    - on top of block cryption functions cipher-modes are build: ecb, ecb_cts, cbc, cbc_cts, cfb, ctr; pcbc, ofb;
    - on top of hash update function hbelt algorithm is built;
    - on top of authentication functions mac algorithm & ae cipher-modes are build: omac, gcm; eax, xts, cwc;
    - additional functions:
    - key expansion;
    - key derivation (key-rep);
    - key-e, key-d - key encryption & decryption;
    */
    struct belt
    {
        typedef std::uint32_t key_t[8];
        typedef std::uint32_t block_t[4];
        typedef block_t iv_t;
        typedef std::uint64_t mac_t;

        //block cipher low-level traits
        void e(key_t k[1], block_t x[1]);
        void d(key_t k[1], block_t x[1]);

        //mac
        void gmac(block_t s[1], block_t x[1]);
        void omac(block_t s[1], block_t x[1]);

        //block cipher modes
        //ecb, ecb_cts, cbc, cbc_cts, ofb, ctr
        //gcm

        //hash
        typedef block_t hash_val_t[2];
        void h(block_t z[3], block_t x[2]);
    };

    /*
    - basic bign functions are:
    - gen private keys/val public keys/val params;
    - sign hash/verify signature;
    - wrap/unwrap token;
    - id-sign hash/id-verify signature;
    */
    template < size_t l >
    struct bign
    {
        static_assert(128 == l || 192 == l || 256 == l, "level 'l' must be 128 or 192 or 256.");

        static size_t const n = 2 * 32 * l / 128;
        typedef std::uint32_t pri_t[n];
        typedef std::uint32_t otk_t[n];
        typedef struct
        {
            std::uint32_t x[n];
            std::uint32_t y[n];
        } pub_t;
        typedef struct
        {
            size_t sz; // 1 < sz < 256
            std::uint8_t o[1]; // o[sz];
        } oid_t;
        typedef std::uint32_t hash_val_t[n];
        typedef std::uint32_t sig_t[n * 3 / 2];

        void sign_otk(pri_t pri[1], otk_t otk[1], hash_val_t h[1], oid_t o[1], sig_t s[1]);
        template < class Rng >
        void sign_rng(pri_t pri[1], Rng rng, hash_val_t h[1], oid_t o[1], sig_t s[1]);
        template < class Rng >
        void sign_orng(pri_t pri[1], Rng rng, hash_val_t h[1], oid_t o[1], sig_t s[1]);

        void verify(pub_t pub[1], hash_val_t h[1], oid_t o[1], sig_t s[1]);

        template < class Rng >
        void wrap_token(pub_t pub[1], Rng rng, std::uint8_t const *key, size_t key_size, std::uint8_t *token);

        static size_t const header_size = 16;
        static size_t const tpub_size = n / 2;
        //constexpr size_t calc_token_size(size_t key_size)
        //{
        //    return key_size + header_size + tpub_size;
        //}
        void unwrap_token(pri_t pri[1], std::uint8_t const *token, size_t token_size, std::uint8_t *key);
    };

    struct test_pod
    {
        typedef std::uint32_t block_t;
        typedef std::uint64_t key_t;

        static void e(key_t const *k, block_t *x)
        {
            *x = (*x ^ static_cast< block_t >(*k)) + static_cast< block_t >(*k >> 32);
        }
        static void d(key_t const *k, block_t *x)
        {
            *x = (*x - static_cast< block_t >(*k >> 32)) ^ static_cast< block_t >(*k);
        }
    };
    //!!
    struct test_carray
    {
        typedef std::uint32_t block_t[2];
        typedef std::uint32_t key_t[4];

        //typedef block_t key_t[2];
        static void e(key_t const *k, block_t *x)
        {
            (*x)[0] = ((*x)[0] ^ (*k)[0]) + (*k)[2];
            (*x)[1] = ((*x)[1] ^ (*k)[1]) + (*k)[3];
            //(*x)[0] = ((*x)[0] ^ (*k)[0][0]) + (*k)[1][0];
            //(*x)[1] = ((*x)[1] ^ (*k)[0][1]) + (*k)[1][1];
        }
        static void e(std::uint32_t const *rk, std::uint32_t *rx)//std::uint32_t *rx)
        {
            key_t const *k = reinterpret_cast< key_t const * >(rk);
            block_t *x = reinterpret_cast< block_t * >(rx);
            e(k, x);
        }
        static void e(key_t const *k, std::uint32_t *rx)//std::uint32_t *rx)
        {
            block_t *x = reinterpret_cast< block_t * >(rx);
            e(k, x);
        }
        static void e(std::uint32_t const *rk, block_t *x)//std::uint32_t *rx)
        {
            key_t const *k = reinterpret_cast< key_t const * >(rk);
            e(k, x);
        }
        static void d(key_t const *k, block_t *x)
        {
            (*x)[0] = ((*x)[0] - (*k)[2]) ^ (*k)[0];
            (*x)[1] = ((*x)[1] - (*k)[3]) ^ (*k)[1];
            //(*x)[0] = ((*x)[0] - (*k)[1][0]) ^ (*k)[0][0];
            //(*x)[1] = ((*x)[1] - (*k)[1][1]) ^ (*k)[0][1];
        }
        static void d(std::uint32_t const *rk, std::uint32_t *rx)
        {
            key_t const *k = reinterpret_cast< key_t const * >(rk);
            block_t *x = reinterpret_cast< block_t * >(rx);
            d(k, x);
        }
        static void d(key_t const *k, std::uint32_t *rx)
        {
            block_t *x = reinterpret_cast< block_t * >(rx);
            d(k, x);
        }
        static void d(std::uint32_t const *rk, block_t *x)
        {
            key_t const *k = reinterpret_cast< key_t const * >(rk);
            d(k, x);
        }

    };

    struct test_struct
    {
        typedef struct s_block
        {
            std::uint32_t a, b;
        } block_t;
        typedef struct
        {
            block_t u, v;
        } key_t;
        static void e(key_t const *k, block_t *x)
        {
            (*x).a = ((*x).a ^ (*k).u.a) + (*k).v.a;
            (*x).b = ((*x).b ^ (*k).u.b) + (*k).v.b;
        }
        static void d(key_t const *k, block_t *x)
        {
            (*x).a = ((*x).a - (*k).v.a) ^ (*k).u.a;
            (*x).b = ((*x).b - (*k).v.b) ^ (*k).u.b;
        }
    };
}

template < class T > struct is_any : std::false_type {};

#pragma region ANY_POD
template < class Rep >
class any_pod_val
{
public:
    typedef Rep rep_t;
    //static_assert(std::is_copy_constructible<Rep>::value, "Rep type must be copy-constructible.");
    //any_pod_val(rep_t const &value) : _value(value) {}
    static_assert(true
        && !std::is_reference<Rep>::value
        && std::is_pod<Rep>::value
        , "Rep type must be pod.");
    any_pod_val(rep_t const &value) { memcpy(&_value, &value, sizeof(_value)); }

    any_pod_val() = default;
    any_pod_val(any_pod_val const &) = default;
    //any_pod_val(any_pod_val &&) = default;
    any_pod_val &operator=(any_pod_val const &) = default;
    //any_pod_val &operator=(any_pod_val &&) = default;

    template < class T >
    any_pod_val(T const &a
        , typename std::enable_if< !std::is_same<any_pod_val, T>::value && is_any<T>::value >::type ** = nullptr
        ) : base()
    {
        memcpy(&_value, a.p(), sizeof(_value));
    }

    template < class T >
    typename std::enable_if< !std::is_same<any_pod_val, T>::value && is_any<T>::value
        , any_pod_val & >::type operator=(T const &a)
    {
        memcpy(&_value, a.p(), sizeof(_value));
        return *this;
    }

    rep_t *p() { return &_value; }
    rep_t const *p() const { return &_value; }
    //private:
    rep_t _value;
};
template < class Rep >
bool operator==(any_pod_val<Rep> const &l, any_pod_val<Rep> const &r)
{
    static_assert(std::is_pod<Rep>::value, "Cannot memcmp Rep type.");
    return 0 == memcmp(l.p(), r.p(), sizeof(Rep));
    //return (*l.p()) == (*r.p());
    return false;
}
template < class Rep >
bool operator!=(any_pod_val<Rep> const &l, any_pod_val<Rep> const &r)
{
    return !(l == r);
}

template < class Rep > struct is_any< any_pod_val<Rep> > : std::true_type{};


template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any &a) {}
template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any const &a) {}
template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any *a) {}
template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any const *a) {}

template < class Any1, class Any2 >
bool equal(Any1, Any2)
{}

template < class Rep >
class any_pod_ref
{
public:
    typedef Rep rep_t;
    //static_assert(std::is_copy_constructible<Rep>::value, "Rep type must be copy-constructible.");
    //any_pod_ref(rep_t const &value) : _value(value) {}
    static_assert(true
        && !std::is_reference<Rep>::value
        && std::is_pod<Rep>::value
        , "Rep type must be pod.");
    //&& std::is_reference<Rep>::value
    //&& std::is_pod<typename std::remove_reference<Rep>::type>::value
    //, "Rep type must be pod reference.");
    any_pod_ref(rep_t &value) : _value(value) {}

    any_pod_ref() = default;
    any_pod_ref(any_pod_ref const &) = default;
    //any_pod_ref(any_pod_ref &&) = default;
    any_pod_ref &operator=(any_pod_ref const &) = default;
    //any_pod_ref &operator=(any_pod_ref &&) = default;

    template < class T >
    any_pod_ref(T const &a
        , typename std::enable_if< !std::is_same<any_pod_ref, T>::value && is_any<T>::value >::type ** = nullptr
        ) : _value(*a.p())
    {
    }

    template < class T >
    typename std::enable_if< !std::is_same<any_pod_ref, T>::value && is_any<T>::value
        , any_pod_ref & >::type operator=(T const &a)
    {
        memcpy(&_value, a.p(), sizeof(_value));
        return *this;
    }

    rep_t *p() { return &_value; }
    rep_t const *p() const { return &_value; }
    //private:
    rep_t &_value;
};
template < class Rep >
bool operator==(any_pod_ref<Rep> const &l, any_pod_ref<Rep> const &r)
{
    static_assert(std::is_pod<Rep>::value, "Cannot memcmp Rep type.");
    return 0 == memcmp(l.p(), r.p(), sizeof(Rep));
    //return (*l.p()) == (*r.p());
    return false;
}
template < class Rep >
bool operator!=(any_pod_ref<Rep> const &l, any_pod_ref<Rep> const &r)
{
    return !(l == r);
}

template < class Rep > struct is_any< any_pod_ref<Rep> > : std::true_type{};

#if 0
template < class Rep >
class any_pod : public std::conditional< std::is_reference<Ref>::value,
    any_pod_ref<Rep>, any_pod_val<Rep> >::type
{
    typedef std::conditional < std::is_reference<Ref>::value,
        any_pod_ref<Rep>, any_pod_val<Rep> > ::type base;
public:
    template <class ...Args>
    any_pod(Args &&...args) : base(std::forward<Args>(args)...) {}
};
#endif
#pragma endregion

#pragma region ANY_CLASS_DATA
template < class Rep > // std::array< std::uint32_t, 2 >, std::vector< std::uint32_t >, std::string
class any_class_data_val
{
public:
    typedef Rep rep_t;
    typedef typename Rep::value_type raw_rep_t;
    //static_assert(std::is_copy_constructible<Rep>::value, "Rep type must be copy-constructible.");
    //any_class_data_val(rep_t const &value) : _value(value) {}
    static_assert(true
        && std::is_class<Rep>::value
        && std::is_copy_constructible<Rep>::value
        //&& std::is_equalable<Rep>::value
        //&& std::has_value_type<Rep>::value
        //&& std::has_data<Rep>::value
        && !std::is_reference<Rep>::value
        , "Rep type must be class.");

    any_class_data_val(rep_t const &value) : _value(value) {}
    any_class_data_val(rep_t &&value) : _value(std::move(value)) {}
    //template < class ...Args >
    //any_class_data_val(Args &&...args) : _value(std::forward<Args>(args)...) {}

    any_class_data_val() = default;
    any_class_data_val(any_class_data_val const &) = default;
    //any_class_data_val(any_class_data_val &&) = default;
    any_class_data_val &operator=(any_class_data_val const &) = default;
    //any_class_data_val &operator=(any_class_data_val &&) = default;

    raw_rep_t *p() { return _value.data(); }
    raw_rep_t const *p() const { return _value.data(); }
    rep_t _value;
};
template < class Rep >
bool operator==(any_class_data_val<Rep> const &l, any_class_data_val<Rep> const &r)
{
    static_assert(std::is_pod<typename any_class_data_val<Rep>::raw_rep_t>::value, "Cannot memcmp Rep type.");
    return 0 == memcmp(l.p(), r.p(), sizeof(Rep));
    //return l._value == r._value;
    //return (*l.p()) == (*r.p());
    return false;
}
template < class Rep >
bool operator!=(any_class_data_val<Rep> const &l, any_class_data_val<Rep> const &r)
{
    return !(l == r);
}

template < class Rep > struct is_any< any_class_data_val<Rep> > : std::true_type{};

template < class Rep > // std::array< std::uint32_t, 2 >, std::vector< std::uint32_t >, std::string
class any_class_data_ref
{
public:
    typedef Rep rep_t;
    typedef typename Rep::value_type raw_rep_t;
    //static_assert(std::is_copy_constructible<Rep>::value, "Rep type must be copy-constructible.");
    //any_class_data_ref(rep_t const &value) : _value(value) {}
    static_assert(true
        && std::is_class<Rep>::value
        && std::is_copy_constructible<Rep>::value
        //&& std::is_equalable<Rep>::value
        //&& std::has_value_type<Rep>::value
        //&& std::has_data<Rep>::value
        && !std::is_reference<Rep>::value
        , "Rep type must be class.");

    any_class_data_ref(rep_t const &value) : _value(value) {}
    any_class_data_ref(rep_t &&value) : _value(std::move(value)) {}
    //template < class ...Args >
    //any_class_data_ref(Args &&...args) : _value(std::forward<Args>(args)...) {}

    any_class_data_ref() = default;
    any_class_data_ref(any_class_data_ref const &) = default;
    //any_class_data_ref(any_class_data_ref &&) = default;
    any_class_data_ref &operator=(any_class_data_ref const &) = default;
    //any_class_data_ref &operator=(any_class_data_ref &&) = default;

    raw_rep_t *p() { return _value.data(); }
    raw_rep_t const *p() const { return _value.data(); }
    rep_t &_value;
};
template < class Rep >
bool operator==(any_class_data_ref<Rep> const &l, any_class_data_ref<Rep> const &r)
{
    static_assert(std::is_pod<typename any_class_data_ref<Rep>::raw_rep_t>::value, "Cannot memcmp Rep type.");
    return 0 == memcmp(l.p(), r.p(), sizeof(Rep));
    //return l._value == r._value;
    //return (*l.p()) == (*r.p());
    return false;
}
template < class Rep >
bool operator!=(any_class_data_ref<Rep> const &l, any_class_data_ref<Rep> const &r)
{
    return !(l == r);
}
#pragma endregion

template < class Rep > struct is_any< any_class_data_ref<Rep> > : std::true_type{};

#pragma region ANY_CLASS_ARRAY

//// Rep: int, int [2], struct { int i; };
//template < class Rep > // std::is_pod< Rep >::value == true
//using any_class_array =
//    typename std::enable_if < std::is_pod< Rep >::value,
//        typename std::conditional <
//            std::is_array< Rep >::value,
//            any_class_data < std::array< typename std::remove_extent< Rep >::type, std::extent< Rep >::value > >,
//            any_class_data < std::array< Rep, 1 > >
//        > ::type
//    > ::type;
template < class Rep > // std::is_pod< Rep >::value == true
using any_class_array_rep =
typename std::enable_if < std::is_pod< Rep >::value,
typename std::conditional <
std::is_array< Rep >::value,
std::array< typename std::remove_extent< Rep >::type, std::extent< Rep >::value >,
std::array< Rep, 1 >
> ::type
> ::type;

template < class T >
struct decay_array
{
    typedef typename std::remove_reference<T>::type U;
    typedef typename std::conditional <
        std::is_array<U>::value,
        typename std::remove_extent<U>::type,
        U > ::type V;
    typedef typename std::remove_cv<V>::type type;
};
template < class T >
using decay_array_t = typename decay_array<T>::type;

template < class Rep >
class any_class_array : public any_class_data_val< any_class_array_rep< Rep > >
{
    typedef any_class_data_val< any_class_array_rep< Rep > > base;
public:
    //typedef typename base::rep_t rep_t;
    typedef any_class_array_rep< Rep > rep_t;

    any_class_array() : base() {}
    any_class_array(any_class_array const &a) : base(a) {}
    any_class_array(rep_t const &r) : base(r) {}
    any_class_array(Rep const &r) : base()
    {
        assign(r);
    }
    any_class_array &operator=(any_class_array const &a)
    {
        this->_value = a._value;
        return *this;
    }
    any_class_array &operator=(rep_t const &r)
    {
        this->_value = r;
        return *this;
    }
    any_class_array &operator=(Rep const &r)
    {
        assign(r);
        return *this;
    }

    template < class T >
    any_class_array(T const &a
        , typename std::enable_if< !std::is_same<any_class_array, T>::value && is_any<T>::value >::type ** = nullptr
        ) : base()
    {
        assign(*a.p());
    }

private:
    //template < class R >
    //void assign(R const &ca
    //    //, typename std::enable_if< std::is_same<std::remove_extent_t<Rep>, std::decay_t<R>>::value && std::is_array<R>::value >::type ** = nullptr
    //    )
    //{
    //    std::cout 
    //        << (std::is_same< R, std::uint32_t const [2] >::value ? "y" : "n") << std::endl
    //        << typeid(std::decay_t<R>).name() << std::endl
    //        ;
    //}
    template < class R >
    void assign(R const &ca
        , typename std::enable_if< std::is_same<std::remove_extent_t<Rep>, decay_array_t<R>>::value && std::is_array<R>::value >::type ** = nullptr
        )
    {
        std::copy(std::begin(ca), std::end(ca), this->_value.begin());
    }
    template < class R >
    void assign(R const &v
        , typename std::enable_if< std::is_same<Rep, decay_array_t<R>>::value && !std::is_array<R>::value >::type ** = nullptr
        )
    {
        this->_value[0] = v;
    }

    //template < class ...Args >
    //any_class_array(Args ...args) : base(std::forward<Args>(args)...) {}
};
#pragma endregion

#pragma region ANY_CLASS_VECTOR
template < class Rep >
using any_class_vector_val =
typename std::conditional <
std::is_array< Rep >::value,
typename std::remove_extent< Rep >::type,
Rep
> ::type;

template < class Rep >
using any_class_vector_rep =
typename std::enable_if < true
&& std::is_pod< Rep >::value
&& std::is_copy_constructible<
any_class_vector_val< Rep >
>::value,
std::vector< any_class_vector_val< Rep > >
> ::type;

template < class Rep >
class any_class_vector : public any_class_data_val< any_class_vector_rep< Rep > >
{
    typedef any_class_data_val< any_class_vector_rep< Rep > > base;
public:
    typedef any_class_vector_rep< Rep > rep_t;
    //template < class ...Args >
    //any_class_vector(Args ...args) : base(std::forward<Args>(args)...) {}

    any_class_vector() : base() {}
#ifdef _DEBUG
    any_class_vector(any_class_vector const &a) : base(a) { check_size(a._value); }
    any_class_vector(any_class_vector &&a) : base(std::move(a)) { check_size(this->_value); }
#else
    any_class_vector(any_class_vector const &a) = default;
    //any_class_vector(any_class_vector &&a) = default;
#endif
    any_class_vector(rep_t const &r) : base(r) { check_size(r); }
    any_class_vector(rep_t &&r) : base(std::move(r)) { check_size(this->_value); }
    any_class_vector(Rep const &r) : base()
    {
        assign(r);
    }
#ifdef _DEBUG
    any_class_vector &operator=(any_class_vector const &a)
    {
        check_size(a._value);
        this->_value = a._value;
        return *this;
    }
    any_class_vector &operator=(any_class_vector &&a)
    {
        check_size(a._value);
        this->_value = std::move(a._value);
        return *this;
    }
#else
    any_class_vector &operator=(any_class_vector const &a) = default;
    //any_class_vector &operator=(any_class_vector &&a) = default;
#endif
    any_class_vector &operator=(rep_t const &r)
    {
        check_size(r);
        this->_value = r;
        return *this;
    }
    any_class_vector &operator=(rep_t &&r)
    {
        check_size(r);
        this->_value = std::move(r);
        return *this;
    }
    any_class_vector &operator=(Rep const &r)
    {
        assign(r);
        return *this;
    }
private:
    static size_t const vec_size = std::is_array<Rep>::value ? std::extent<Rep>::value : 1;
    void check_size(rep_t const &r)
    {
        assert(r.size() == vec_size);
    }

    template < class R >
    void assign(R const &ca
        , typename std::enable_if< std::is_same<std::remove_extent_t<Rep>, decay_array_t<R>>::value && std::is_array<R>::value >::type ** = nullptr
        )
    {
        this->_value.resize(vec_size);
        std::copy(std::begin(ca), std::end(ca), this->_value.begin());
    }
    template < class R >
    void assign(R const &v
        , typename std::enable_if< std::is_same<Rep, decay_array_t<R>>::value && !std::is_array<R>::value >::type ** = nullptr
        )
    {
        this->_value.resize(vec_size);
        this->_value[0] = v;
    }
};
#pragma endregion

//template < class Rep >
//class any : public std::conditional< std::is_reference<Rep>::value, 
//    typename std::conditional < std::is_pod<Rep>::value,
//        any_pod_ref<Rep>,
//        any_class<Rep> >::type, 
//    typename std::conditional< std::is_pod<Rep>::value, 
//        any_pod_val<Rep>, 
//        any_class<Rep> >::type >::type
//{
//public:
//};

#if 0
template < class T, class Impl >
class any_val
{
    std::uint8_t _buf[sizeof(T)];
    T *_val() { return reinterpret_cast<T *>(_buf); }
    T const *_val() const { return reinterpret_cast<T const *>(_buf); }
    Impl *_this() { return static_cast<Impl *>(this); }
    Impl const *_this() const { return static_cast<Impl const *>(this); }
public:
    template < class ...Args >
    any_val(Args &&...args)
    {
        _this()->construct(std::forward<Args>(args)...);
    }
    ~any_val()
    {
        _val()->~T();
    }
};
template < class T >
class any_ref
{
public:
    T &_value;
    any_ref(T &v) : _value(v) {}
};
template < class T >
class any_dkey
{
public:
    //dkey_ptr _dkey;
};
template < class T >
class any_secure
{
public:
    //handle_t h;
};

template < class T >
class any_pod
{
public:

};
template < class T, template <class> class Storage >
class any_class
{
};

template < class T, template <class> class Storage >
class any
{
};
#endif

void test_any_class_array()
{
    any_class_array< int > i;
    bool ok = i == i;
    static_assert(std::is_same< decltype(i._value), std::array< int, 1 > >::value, "array is not same int");

    struct s_test { int i; };
    any_class_array< s_test > s;
    static_assert(std::is_same< decltype(s._value), std::array< s_test, 1 > >::value, "array is not same struct");

    any_class_array< int[2] > a;
    //static_assert(std::is_same< decltype(a._value), std::array< int [2], 1 > >::value, "");
    static_assert(std::is_same< decltype(a._value), std::array< int, 2 > >::value, "array is not same array");
}

void test_any_class_vector()
{
    any_class_vector< int > vi;
    bool ok = vi == vi;
    static_assert(std::is_same< decltype(vi._value), std::vector< int > >::value, "vector is not same int");
}

//void test_any_class_ref
void test_const()
{
    int i = 1;

    int const *cp = &i;
    //*cp = 2;
    cp++;

    int *const pc = &i;
    *pc = 3;
    //pc++;

    int const *const cpc = &i;
    //*cpc = 4;
    //cpc++;

    int a[2];
    int(*pa)[2] = &a;
    int const (*cpa)[2] = &a;
    int(*const pca)[2] = &a;
    int const(*const cpca)[2] = &a;

}

template < template < class > class F, class X >
class test_using
{
public:
    F<X> v;
};
void test_test_using()
{
    test_using< any_class_array, int > ai;
    ai.v._value[0] = 1;
    test_using< any_class_array, int[2] > aa;
    aa.v._value[1] = 2;
    test_using< any_pod_val, int > pi;
    pi.v._value = 2;
    test_using< any_pod_val, int[2] > pa;
    pa.v._value[1] = 2;

    //test_using< any_class_vector, int > vi;    
    //vi.v._value[0] = 1;
}

template < class Rep >
using data_block = any_pod_val< Rep >;

class data_stream {};

namespace any {
    // std::vector<int>, int [2], int &, ...

    template < class RawRep > // int, std::uint32_t, char, int *
    class raw_any
    {
    public:
        // TODO replace is_pod with apropriate traits
        static_assert(std::is_pod<RawRep>::value, "RawRep typename must be a pod.");
        typedef RawRep rep_t;

        raw_any() = default;
        raw_any(raw_any const &o) = default;
        //raw_any(raw_any &&o) = default;
        raw_any &operator=(raw_any const &o) = default;
        //raw_any &operator=(raw_any &&o) = default;

        raw_any(rep_t const &v) : _v(v) {}

    private:
        rep_t _v;
    };

#if 0
    template < class Pod > // int, std::uint32_t, char, int *
    class raw_any_pod
    {
    public:
        // TODO replace is_pod with apropriate traits
        static_assert(std::is_pod<Pod>::value, "Pod typename must be a pod.");
        typedef Pod rep_t;

        raw_any_pod() = default;
        raw_any_pod(raw_any_pod const &o) = default;
        raw_any_pod(raw_any_pod &&o) = default;
        raw_any_pod &operator=(raw_any_pod const &o) = default;
        raw_any_pod &operator=(raw_any_pod &&o) = default;

        raw_any_pod(rep_t const &v) : _v(v) {}

    private:
        rep_t _v;
    };
    template < class PodCarray > // int [2], char [8], std::uint32_t * [2], //"asdfsdf"
    class raw_any_pod_carray
    {
    public:
        static_assert(std::is_array<PodCarray>::value &&
            std::is_pod<typename std::remove_extent<PodCarray>::type>::value, "PodCarray typename must be a pod array.");
        typedef PodCarray rep_t;

        raw_any_pod_carray() = default;
        raw_any_pod_carray(raw_any_pod_carray const &o) = default; // TODO memcpy?
        raw_any_pod_carray(raw_any_pod_carray &&o) = default;
        raw_any_pod_carray &operator=(raw_any_pod_carray const &o) = default;
        raw_any_pod_carray &operator=(raw_any_pod_carray &&o) = default;

        raw_any_pod_carray(rep_t const &v) : _v(v) {}
    private:
        rep_t _v;
    };
    template < class Struct >
    class raw_any_struct
    {
    public:
        //TODO
        static_assert(true
            && std::is_class<Struct>::value
            && std::is_trivial<Struct>::value
            && std::is_trivially_constructible<Struct>::value // Struct()
            && std::is_trivially_copy_assignable<Struct>::value // Struct u, v; v = u;
            && std::is_trivially_copy_constructible<Struct>::value // Struct u, v(u);
            , "Struct typename must be a trivial.");
        typedef Struct rep_t;

        raw_any_struct() = default;
        raw_any_struct(raw_any_struct const &o) = default; // TODO memcpy?
        raw_any_struct(raw_any_struct &&o) = default;
        raw_any_struct &operator=(raw_any_struct const &o) = default;
        raw_any_struct &operator=(raw_any_struct &&o) = default;

        raw_any_struct(rep_t const &v) : _v(v) {}

    private:
        rep_t _v;
    };
#endif

    template < class Rep, class Traits >
    class any {};

    template < class Any >
    struct any_traits
    {};

    template < class RawRep >
    int test_raw_any()
    {
        raw_any< RawRep > r;
        return 0;
    }
    template < class ...RawReps >
    void test_raw_anys()
    {
        int force_eval[] = { test_raw_any< RawReps >()... };
    }
    struct empty {};
    void test_raw_any()
    {
        test_raw_anys< int, std::uint32_t, int[2], empty>();
    }
}


template < class Alg, class KeyTag, class PlainTag >
struct Encrypted {};


namespace symmetric {



#if 0
    void encrypt_test()
    {
        typedef int belt_block_t[4];
        typedef int belt_key_t[8];

        emode mode = ecb;

        belt_key_t k[1];
        belt_block_t iv[1];
        std::vector< std::uint8_t > x(10, 10), y;

        // 1. sizeof output?
        y.resize(x.size());

        // 2. block count
        size_t blocks = x.size() / sizeof(belt_block_t);
        // 2.1. tail?
        size_t tail_bits = 8 * (x.size() % sizeof(belt_block_t));

        // 3. checks
        if (mode == ecb && tail_bits != 0)
            throw std::logic_error();
        if (mode == ecb_cts && tail_bits != 0 && blocks == 0)
            throw std::logic_error();

        // 4. aligned?
        if (aligned(x.data()))
        {
            belt_block_t const *px = (belt_block_t const *)x.data();

            if (aligned(y.data()))
            {
                belt_block_t *py = (belt_block_t *)y.data();

                switch (mode)
                {
                case ecb:
                    // 5. for all blocks
                    for (; blocks--; ++px, ++py)
                    {
                        // 5.1. load
                        *py = *px; // memcpy
                        // 5.2. crypt
                        e(k, py);
                        // 5.3. save
                        // 5.4. increment
                    }
                    break;
                case ecb_cts:
                    for (; blocks-- > 1; ++px, ++py)
                    {
                        *py = *px; // memcpy
                        e(k, py);
                    }
                    if (tail_bits != 0)
                    {
                        *py = *px; // memcpy tail_bits
                        e(k, py);
                        block_t t[1];
                        ++px;
                        set_block_left(t, px, tail_bits);
                        auto rtail_bits = 8 * sizeof(block_t) - tail_bits;
                        set_block_right(t, py, rtail_bits);
                        e(k, t);
                        set_block_left(py + 1, py, tail_bits);
                        *py = *t;
                    }
                    else
                    {
                        *py = *px; // memcpy
                        e(k, py);
                    }
                    break;
                case cbc:
                    for (; blocks--;)
                    {
                        *py = *px ^ *iv; // memcpy
                        e(k, py);
                        *iv = *py;
                    }
                case cbc_cts:
                    for (; blocks--;)
                    {
                        *py = *px ^ *iv; // memcpy
                        e(k, py);
                        *iv = *py;
                    }
                    if (tail_bits != 0)
                    {

                    }

                    break;
                }
            }
            else
            {
                auto bx = x.data();
                belt_block_t p[1];

                if (!aligned(y.data()))
                {
                    auto by = y.data();

                    switch (mode)
                    {
                    case ecb:
                        for (; blocks--; bx += sizeof(belt_block_t), by += sizeof(belt_block_t))
                        {
                            load(p, bx);
                            e(k, p);
                            save(by, p);
                        }
                        break;
                    case ecb_cts:
                        for (; blocks-- > 1; ++px, ++py)
                        {
                            *py = *px; // memcpy
                            e(k, py);
                        }
                        if (tail_bits != 0)
                        {
                            *py = *px; // memcpy tail_bits
                            e(k, py);
                            block_t t[1];
                            ++px;
                            set_block_left(t, px, tail_bits);
                            auto rtail_bits = 8 * sizeof(block_t) - tail_bits;
                            set_block_right(t, py, rtail_bits);
                            e(k, t);
                            set_block_left(py + 1, py, tail_bits);
                            *py = *t;

                            aligned

                        }
                        else
                        {
                            *py = *px; // memcpy
                            e(k, py);
                        }
                        break;
                    case cbc:
                        for (; blocks--;)
                        {
                            *py = *px ^ *iv; // memcpy
                            e(k, py);
                            *iv = *py;
                        }
                    case cbc_cts:
                        for (; blocks--;)
                        {
                            *py = *px ^ *iv; // memcpy
                            e(k, py);
                            *iv = *py;
                        }
                        if (tail_bits != 0)
                        {

                        }

                        break;
                    }
                }

            }
        else
        {

        }
        }
#endif

        //namespace block {
        //
        //    //class belt
        //    //{
        //    //public:
        //    //    boost::mpl::vector< ecb_cts, cbc_cts, ctr, cfb, omac, gcm> supported_modes;
        //    //    boost::mpl::fmap < cipher< belt_traits, _1 >, supported_modes > supported_ciphers;
        //    //};
        //
        //    template < class Alg >
        //    class cipher: public Alg 
        //    {
        //    public:
        //        void encrypt_next();
        //        void decrypt_next();
        //    };
        //
        //    template < class Alg >
        //    class ecb_cts : public Alg 
        //    {
        //    public:
        //        typedef typename Alg::key_t key_t;
        //        typedef typename Alg::block_t block_t;
        //        void encrypt_next();
        //        void encrypt_cts(key_t const k[1], block_t py[1], block_t y[1], block_t const x[1], size_t tail_bits)
        //        {
        //        }
        //    };
        //
        //    class hash
        //    {
        //        typedef int block_t;
        //        typedef int val_t;
        //
        //        void init();
        //        void next(block_t const *x);
        //        void last(block_t const *x, size_t tail_bits);
        //        void done();
        //        void val(val_t *y);
        //        void clear();
        //        void clone(hash *h) const;
        //    };
        //    template < class Hash >
        //    class hmac
        //    {
        //        typedef typename Hash::block_t block_t;
        //        typedef typename Hash::val_t val_t;
        //
        //        void init();
        //        void next(block_t const *x);
        //        void last(block_t const *x, size_t tail_bits);
        //        void done();
        //        void val(val_t *y);
        //        void clear();
        //        void clone(hmac<Hash> *h) const;
        //    };
        //
        //}
        //namespace blocks {
        //
        //    template < class BlockAlg > // block::cipher< belt, ecb >
        //    class blocks
        //    {
        //    public:
        //        template < class IBlocks, class OBlocks >
        //        void encrypt(IBlocks ibegin, IBlocks iend, OBlocks out, size_t tail_bits)
        //        {
        //            block_t y[1];
        //            for (; ibegin != iend; ++ibegin)
        //            {
        //                BlockAlg::e(*ibegin, y);
        //                *out++ = *y;
        //            }
        //            BlockAlg::e_last(*ibegin, y, tail_bits);
        //            *out++ = std::make_pair(*y, tail_bits);
        //        }
        //    };
        //}
        //namespace buffered {
        //    template < class BlocksAlg >
        //    class buffered {
        //        block_t _buf[1024];
        //        size_t _off, _sz;
        //    public:
        //        void encrypt( std::uint8_t const *x, std::uint8_t *y, size_t sz )
        //        {
        //            sz = std::min(sz, sizeof(_buf));
        //            memcpy(_buf, x, sz);
        //            BlocksAlg::encrypt(_buf, _buf + _off);
        //        }
        //    };
        //
        //}
        //namespace stream {
        //    std::ifstream;
        //    std::ofstream;
        //    boost::asio::ip::tcp::socket s;
        //
        //    void test()
        //    {
        //        struct MyPlainData {};
        //        async_stream< std::ifstream, MyPlainData > fin("fin.txt");
        //        typedef dh< Myself<Pri>, Bob<TransportPub> > dh_t;
        //        typedef key<belt, Shared< Myself<Pri>, Bob<TransportPub>, DataEncryptionKey> key_t;
        //
        //        key_t k = dh.eval( );
        //        async_stream< std::ofstream, 
        //            concat< 
        //                signed< bign, MyPrivateKey, hashed< hbelt, MyPlainData > >, 
        //                encrypted<belt, Shared< MyPri, BobsPub, DataEncryptionKey>, MyPlainData> > > fout("fout.txt");
        //
        //        //k(fin);
        //        //cat fin.txt | dkey encrypt --key K --plain-stdin --cipher-stdout > fout.txt
        //
        //        auto op = fin >> (h & k) >> std::make_pair( sign(h.value()), id) >> (\x -> x.second || x.first) >> send_to_bob;
        //        op.eval();
        //        /*
        //        */
        //    }
        //
        //
        //    template < class LHS, class RHS >
        //    typename std::enable_if< true
        //        && is_istream<LHS> && is_key<RHS>
        //        , apply< LHS, RHS >
        //    >::type operator>>(LHS &lhs, RHS &rhs)
        //    {
        //        return apply_key_to_stream(lhs, rhs);
        //    }
        //    template < class LHS, class RHS >
        //    typename std::enable_if< true
        //        && is_apply<LHS> && is_ostream<RHS>
        //        , RHS
        //    >::type operator>>(LHS &lhs, RHS &rhs)
        //    {
        //        for (; !lhs.empty(); )
        //            rhs.put_val( lhs.get_val() );
        //    }
        //
        //}

    }

    namespace asymmetric
    {
        class pri
        {
        public:
        };

        void test()
        {
            //key k;
            //k.e();
            //hash h;
            //auto hash_value = h.hash_data( data );
        }

        class sign;
        class dh;
        class id_sign;


        class bpace;
        class bmqv;
        class bsts;

        class btls;
    }

    template < class Alg, template < class > class KeyRep = any_pod_val >
    class key : public KeyRep< typename Alg::key_t >
    {
        typedef KeyRep< typename Alg::key_t > base;
        typedef typename Alg::key_t key_t;
        typedef typename Alg::block_t block_t;
    public:
        typedef typename base::rep_t rep_t;
        static_assert(std::is_pod<key_t>::value, "key_t type must be pod.");
        //key(rep_t const &key) : base(key) {}
        template < class ...Args >
        key(Args &&...args) : base(std::forward<Args>(args)...){}

        template < class Arg >
        key &operator=(Arg &&arg)
        {
            static_cast<base &>(*this) = std::forward<Arg>(arg);
            return *this;
        }

        //template < class Plain, class Cipher >
        //typename std::enable_if< is_key<Plain>::value >::type
        //    ke( Plain const &plain, Cipher const &cipher )
        //{
        //    auto x = get_key_bytes(plain);
        //    auto x = plain.get_key_bytes();
        //}

        //template < class PlainTag >
        //using encrypted = Encrypted < Alg, KeyTag, PlainTag > ;

        //template < class PlainTag, template < class > class PlainRep /*= any_pod_val*/, template < class > class CipherRep = PlainRep >
        //void e(tagged< PlainTag, PlainRep<block_t> > const &plain, tagged< encrypted< PlainTag >, CipherRep<block_t> > &cipher) const
        //{}
        //template < class Plain, class Cipher >
        //template std::enable_if< true
        //    && std::is_same< typename get_tag<Cipher>::type , encrypted<typename get_tag<Plain>::type> >::value
        //>::type e(Plain const &plain, Cipher &cipher) const
        //{
        //    assign(cipher, plain);
        //    Alg::e(get_ptr(*this), get_ptr(cipher));
        //}

        template < template < class > class PlainRep /*= any_pod_val*/, template < class > class CipherRep = PlainRep >
        void e(PlainRep<block_t> const &plain, CipherRep<block_t> &cipher) const
        {
            cipher = plain;
            Alg::e(this->p(), cipher.p());
        }
#if 1
#if 0
        template < template < class > class DataRep /*= any_pod_val*/, bool safe_move = false >
        typename std::conditional< safe_move, DataRep<block_t>, DataRep<block_t> & >::type
            e(DataRep<block_t> &&plain) const
#else
        template < bool safe_move, template < class > class DataRep /*= any_pod_val*/ >
        typename std::conditional< safe_move, DataRep<block_t>, DataRep<block_t> & >::type
            e(DataRep<block_t> &&plain) const
#endif
        {
            if (safe_move)
            {
                DataRep<block_t> cipher(std::move(plain));
                Alg::e(this->p(), cipher.p());
                return cipher;
            }
            else
            {
                Alg::e(this->p(), plain.p());
                return plain;
            }
        }
#else
        data_block<block_t> & e(data_block<block_t> &&plain)
        {
            Alg::e(this->p(), plain.p());
            return plain;
        }
#endif
        template < template < class > class PlainRep /*= any_pod_val*/, template < class > class CipherRep = PlainRep >
        CipherRep<block_t> e(PlainRep<block_t> const &plain) const
        {
            CipherRep<block_t> cipher = plain;
            Alg::e(this->p(), cipher.p());
            return cipher;
        }
        template < template < class > class DataRep /*= any_pod_val*/ >
        //typename std::conditional< false, DataRep<block_t>, DataRep<block_t> & >::type
        DataRep<block_t> &
            e(DataRep<block_t> &&plain) const
        {
            return e< false >(std::move(plain));
        }

        //template < template < class > class CipherRep /*= any_pod_val*/, template < class > class PlainRep = CipherRep >
        //void d(CipherTag<CipherRep<block_t>> const &cipher, PlainRep<block_t> &plain) const
        //{
        //    plain = cipher;
        //    Alg::d(this->p(), plain.p());
        //}
#if 1
#if 0
        template < template < class > class DataRep /*= any_pod_val*/, bool safe_move = false >
        typename std::conditional< safe_move, DataRep<block_t>, DataRep<block_t> & >::type
            d(DataRep<block_t> &&cipher) const
#else
        template < bool safe_move, template < class > class DataRep /*= any_pod_val*/ >
        typename std::conditional< safe_move, DataRep<block_t>, DataRep<block_t> & >::type
            d(DataRep<block_t> &&cipher) const
#endif
        {
            if (safe_move)
            {
                DataRep<block_t> plain(std::move(cipher));
                Alg::d(this->p(), plain.p());
                return plain;
            }
            else
            {
                Alg::d(this->p(), cipher.p());
                return cipher;
            }
        }
#else
        data_block<block_t> &d(data_block<block_t> &&cipher)
        {
            Alg::d(this->p(), cipher.p());
            return cipher;
        }
#endif
        template < template < class > class CipherRep /*= any_pod_val*/, template < class > class PlainRep = CipherRep >
        PlainRep<block_t> d(CipherRep<block_t> const &cipher) const
        {
            PlainRep<block_t> plain = cipher;
            Alg::d(this->p(), plain.p());
            return plain;
        }
        template < template < class > class DataRep /*= any_pod_val*/ >
        //typename std::conditional< false, DataRep<block_t>, DataRep<block_t> & >::type
        DataRep<block_t> &
            d(DataRep<block_t> &&cipher) const
        {
            return d< false >(std::move(cipher));
        }

#ifdef TEMPLATES
        template < class PlainRep, class PlainTag, class Log, class CipherRep >
        void e(data_block< PlainRep, PlainTag, Log > const &plain, data_block< CipherRep, tag::encrypted_block< Alg, Tag, PlainTag >, Log > &cipher)
        {
            //cipher = plain;
            //Alg::e( base::p(), cipher.p() );
        }

        template < class Rep, class PlainTag, class Log >
        data_block< Rep, tag::encrypted_block< Alg, Tag, PlainTag >, Log > &e(data_block< Rep, PlainTag, Log > &&plain)
        {
            //auto &cipher = *reinterpret_cast< data_block< Rep, tag::encrypted_block< Alg, Tag, PlainTag >, Log >* >(&plain);
            //Alg::e(base::p(), cipher.p());
            //return cipher;
            return *reinterpret_cast< data_block< Rep, tag::encrypted_block< Alg, Tag, PlainTag >, Log >* >(&plain);
        }

        template < class PlainRep, class PlainTag, class Log, class CipherRep = PlainRep >
        data_block< CipherRep, tag::encrypted_block< Alg, Tag, PlainTag >, Log > e(data_block< PlainRep, PlainTag, Log > const &plain)
        {
            //data_block< CipherRep, tag::encrypted_block< Alg, Tag, PlainTag >, Log > cipher(plain);
            //Alg::e(base::p(), cipher.p());
            //return cipher;
            return data_block< CipherRep, tag::encrypted_block< Alg, Tag, PlainTag >, Log >();
        }


        template < class CipherRep, class PlainTag, class Log, class PlainRep >
        void d(data_block< CipherRep, tag::encrypted_block< Alg, Tag, PlainTag >, Log > const &cipher, data_block< PlainRep, PlainTag, Log > &plain)
        {
        }

        template < class Rep, class PlainTag, class Log >
        data_block< Rep, PlainTag, Log > &d(data_block< Rep, tag::encrypted_block< Alg, Tag, PlainTag >, Log > &&cipher)
        {
            return data_block< Rep, PlainTag, Log >();
        }

        template < class CipherRep, class PlainTag, class Log, class PlainRep = CipherRep >
        data_block< PlainRep, PlainTag, Log > d(data_block< CipherRep, tag::encrypted_block< Alg, Tag, PlainTag >, Log > const &cipher)
        {
            return data_block< PlainRep, PlainTag, Log >();
        }
#endif
    };


    template < class Alg, template < class > class BlockRep, class Key >
    void test_alg_block_key(
        BlockRep< typename Alg::block_t > &x, Key const &k
        )
    {
        auto ex = k.e(x);
        auto dex = k.d(ex);
        assert(dex == x);

        auto *t = &x;
        auto &rex = k.e(std::move(*t));
        auto &rdex = k.d(std::move(rex));
        assert(rdex == dex);

        t = &rdex;
        auto &srex = k.e<true>(std::move(*t));
        auto &srdex = k.d<true>(std::move(srex));
        assert(srdex == dex);

        t = &srdex;
        auto &urex = k.e<false>(std::move(*t));
        auto &urdex = k.d<false>(std::move(urex));
        assert(urdex == dex);
    }



    template < class Tag, class AnyRep >
    class tagged : AnyRep
    {
        tagged();
    public:
    };

    namespace tag {
        struct data {};
        struct key_encryption_key_tag {} kek;
    }
    tag::data tag_data;

    template < class Rep, class Tag >
    tagged<Rep, Tag> const &add_tag(Rep const &v, Tag t)
    {
        return *reinterpret_cast<tagged<Rep, Tag> const *>(&v);
    }
    template < class Rep, class Tag >
    tagged<Rep, Tag> &add_tag(Rep &v, Tag t)
    {
        return *reinterpret_cast<tagged<Rep, Tag> const *>(&v);
    }
    template < class Tag, class Rep >
    tagged<Rep, Tag> const &add_tag(Rep const &v)
    {
        return *reinterpret_cast<tagged<Rep, Tag> const *>(&v);
    }
    template < class Tag, class Rep >
    tagged<Rep, Tag> &add_tag(Rep &v)
    {
        return *reinterpret_cast<tagged<Rep, Tag> const *>(&v);
    }

    void test_tagged()
    {
        //int i;
        //auto &ti = add_tag<tag::data_tag>(i);
        //int plain;
        //auto &tplain = add_tag<tag::data_tag>(plain);
        //key k;
        //auto cipher = k.e(tplain);
    }

    //template < class CipherRep, class CipherTag, class PlainRep, class PlainTag >
    //void e(add_tag< CipherRep, CipherTag > const &cipher,  ){}

    //template < class Rep, class Tag >
    //struct add_tag
    //{
    //    typedef Rep type;
    //};

    template < class Alg, class Key >
    void test_alg_key(
        typename Alg::block_t &xblock, Key const &k
        )
    {
        {
            typedef any_pod_val<typename Alg::block_t> block_t;
            auto x = block_t(xblock);
            test_alg_block_key< Alg, any_pod_val, Key >(x, k);
        }
    {
        typedef any_class_array<typename Alg::block_t> block_t;
        //typename block_t::rep_t ablock;
        //memcpy(std::addressof(ablock), &xblock, sizeof(xblock));
        //auto x = block_t(ablock);
        auto x = block_t(xblock);
        test_alg_block_key< Alg, any_class_array, Key >(x, k);
    }
    {
        typedef any_class_vector< typename Alg::block_t> block_t;
        //typename block_t::rep_t vblock;
        //memcpy(std::addressof(vblock), &xblock, sizeof(xblock));
        auto x = block_t(xblock);
        test_alg_block_key< Alg, any_class_vector, Key >(x, k);

        //typedef any_class_vector<typename Alg::block_t> block_t;
        //auto x = block_t(xblock);
        //test_alg_block_key< Alg, any_class_vector, Key >(x, k);

    }
    {
        typedef any_pod_ref<typename Alg::block_t> block_t;
        auto x = block_t(xblock);
        test_alg_block_key< Alg, any_pod_ref, Key >(x, k);
    }

    }

    template < class Alg >
    int test_alg(
        typename Alg::block_t &xblock, typename Alg::key_t const &kkey
        )
    {
        {
            typedef key< Alg > key_t;
            auto k = key_t(kkey);
            test_alg_key< Alg, key_t >(xblock, k);
        }
    {
        typedef key< Alg, any_pod_val > key_t;
        auto k = key_t(kkey);
        test_alg_key< Alg, key_t >(xblock, k);
    }
    {
        typedef key< Alg, any_class_array > key_t;
        //typename key_t::rep_t akey;
        //memcpy(std::addressof(akey), &kkey, sizeof(kkey));
        auto k = key_t(kkey);
        test_alg_key< Alg, key_t >(xblock, k);
    }
    {
        typedef key< Alg, any_class_vector > key_t;
        auto k = key_t(kkey);
        test_alg_key< Alg, key_t >(xblock, k);
    }

    //typedef key< alg, tag, rep1 > key1_t;
    //typedef key< alg, tag, rep2 > key2_t;
    //key1_t k1;
    //key2_t k2;
    //k1 = k2;

    return 0;
    }
    template < class Alg >
    int test_alg1(
        std::tuple< typename Alg::block_t, typename Alg::key_t  > const &args
        )
    {
        auto const &kkey = std::get<1>(args);
        auto xblock = std::get<0>(args);
        return test_alg< Alg >(xblock, kkey);
    }
    template < class Alg >
    typename std::enable_if< true
        && std::is_pod< typename Alg::block_t >::value
        && std::is_pod< typename Alg::key_t >::value
        , int >::type test_alg0()
    {
        union
        {
            typename Alg::block_t xblock;
            std::uint8_t xbytes[sizeof(typename Alg::block_t)];
        };
        union
        {
            typename Alg::key_t kkey;
            std::uint8_t kbytes[sizeof(typename Alg::key_t)];
        };

        std::default_random_engine e;
        std::independent_bits_engine< std::default_random_engine, 8, std::uint32_t > rng(e);
        std::generate(std::begin(xbytes), std::end(xbytes), std::ref(rng));
        std::generate(std::begin(kbytes), std::end(kbytes), std::ref(rng));

        //return test_alg< Alg >(std::forward_as_tuple(std::move(xblock), std::move(kkey)));
        //return test_alg< Alg >(std::forward_as_tuple(xblock, kkey));
        return test_alg< Alg >(xblock, kkey);
    }


    template < class ...Algs, class ...Args >
    void test_keys(Args &&...args)
    {
        int force_eval[] = { test_alg1< Algs >(std::forward<Args>(args))... };
        int force_eval0[] = { test_alg0< Algs >()... };
    }
    void test_key()
    {
        test_alg0< impl::test_carray >();
        test_keys<
            impl::test_pod,
            impl::test_struct/*,
                             impl::test_carray*/>(
                             std::make_tuple(1, 2),
                             std::make_tuple(impl::test_struct::block_t(), impl::test_struct::key_t())
                             );
        //{ 1, 2 }, 
        //{ { 3, 4 }, { {5,6}, {7,8} } });
    }

    void test_array()
    {
        std::cout
            << (std::is_pod<int[2]>::value ? "pod" : "no-pod") << std::endl
            << (std::is_array<int[2]>::value ? "array" : "no-array") << std::endl
            << (std::is_copy_constructible<int[2]>::value ? "copy_constructible" : "no-copy_constructible") << std::endl
            << (std::is_trivially_copy_constructible<int[2]>::value ? "trivially_copy_constructible" : "no-trivially_copy_constructible") << std::endl
            << std::endl;
    }

    //int _tmain(int argc, _TCHAR* argv[])
    //{
    //    
    //    std::cout << std::is_pod< std::vector<int> >::value << std::endl;
    //    std::cout << std::is_pod<std::string>::value << std::endl;
    //
    //    test_test_using();
    //    //test_array();
    //    test_key();
    //    return 0;
    //
    //
    //    any::test_raw_any();
    //
    //    auto a = 1;
    //    std::vector<uint16_t> data;
    //    //auto plain = data_block<rep::array, tag::default::plain_data, logs::nolog>();    
    //    //
    //    //assert( plain == ked );
    //
    //    //auto cipher = k.e(std::move(plain));
    //    //auto plain2 = k.d(std::move(cipher));
    //    //auto plain3 = k.d(cipher);
    //    //assert(plain3 == plain2);
    //
    //	return 0;
    //}

    // specific application-level type information
    namespace tag {
        template < class Alg, class KeyTag, class PlainTag >
        struct encrypted_block {};

        namespace default {
            struct plain_data {};
            struct data_encryption_key {};
            struct key_encryption_key {};
            struct key_protection_password {};
        }
    }


    namespace logs
    {
        struct nolog {};

        /*
        Obj - object being logged;
        Impl - logger implementation abstraction, used internally to perform logging actions;
        */
        template < class Obj, class Impl >
        class doclog {
        private:
            Impl *_log() { return static_cast<Impl *>(this); }
            Impl const *_log() const { return static_cast<Impl const *>(this); }
            static std::string _obj_name() { return typeid(Obj).name(); }
        public:
            doclog() { *_log() << _obj_name() << " created"; }
            ~doclog() { *_log() << _obj_name() << " destroyed"; }

            void printf() {}
        };
    }

    namespace rep {

        //may be base struct for all?
        template<typename Type>
        struct autodetect
        {
            Type _value;
        };
        //bool operator==(autodetect &l, autodetect &r){ return true; }
        //template<
        //    class T,
        //        std::size_t N
        //>
        //struct vector //: autodetect 
        //{
        //public:
        //    N _size;
        //    T value;
        //    vector();
        //    bool operator==(autodetect &l, autodetect &r){ return true; }
        //};         

        struct vector{};
        struct array{};

        struct object {};
        struct carray {};
        struct pod {};

        template < class ObjectRep >
        class Object : public ObjectRep
        {
        };
        template < class PodRep > //int, uint32_t, char, ...
        class Pod
        {
        public:
            PodRep _value;
        };
        template < class RefRep >
        class Ref
        {
        public:
            RefRep &_lvalue;
        };

        void test()
        {
            int i = 0;
            auto r = std::ref(i);
            //std::is_trivial
        }



        struct secure {};

        struct dkey {};

        template < class Rep >
        struct ref {};
        template < class Rep >
        struct cref {};
    }

    void test_any()
    {

    }

    void test_alg()
    {
    }
    void test()
    {
        test_any();
        test_alg();
        test_key();
    }

