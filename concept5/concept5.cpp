#include <type_traits>
#include <vector>
#include <array>
#include <string>
#include <iostream>
#include <tuple>
#include <xmmintrin.h>
#include <cstdint>
#include <cassert>
#include <random>

#pragma region DATA

class empty_key
{
};
class any_data
{
public:
    //data(){};
};
class encrypted_data
{
public:
    encrypted_data(){};
};


//Some input types
template < class T > struct is_any : std::false_type {};

template < class Rep >
class any_pod_val
{
public:
    typedef Rep rep_t;
    //static_assert(std::is_copy_constructible<Rep>::value, "Rep type must be copy-constructible.");
    //any_pod_val(rep_t const &value) : _value(value) {}
    static_assert(true
        && !std::is_reference<Rep>::value
        && std::is_pod<Rep>::value
        , "Rep type must be pod.");
    any_pod_val(rep_t const &value) { memcpy(&_value, &value, sizeof(_value)); }

    any_pod_val() = default;
    //any_pod_val(any_pod_val const &) = default;
    //any_pod_val(any_pod_val &&) = default;
    //any_pod_val &operator=(any_pod_val const &) = default;
    //any_pod_val &operator=(any_pod_val &&) = default;

    template < class T >
    any_pod_val(T const &a
        , typename std::enable_if< !std::is_same<any_pod_val, T>::value && is_any<T>::value >::type ** = nullptr
        ) : base()
    {
        memcpy(&_value, a.p(), sizeof(_value));
    }

    template < class T >
    typename std::enable_if< !std::is_same<any_pod_val, T>::value && is_any<T>::value
        , any_pod_val & >::type operator=(T const &a)
    {
        memcpy(&_value, a.p(), sizeof(_value));
        return *this;
    }

    rep_t *p() { return &_value; }
    rep_t const *p() const { return &_value; }
    //private:
    rep_t _value;
};
template < class Rep >
bool operator==(any_pod_val<Rep> const &l, any_pod_val<Rep> const &r)
{
    static_assert(std::is_pod<Rep>::value, "Cannot memcmp Rep type.");
    return 0 == memcmp(l.p(), r.p(), sizeof(Rep));
    //return (*l.p()) == (*r.p());
    return false;
}
template < class Rep >
bool operator!=(any_pod_val<Rep> const &l, any_pod_val<Rep> const &r)
{
    return !(l == r);
}

template < class Rep > struct is_any< any_pod_val<Rep> > : std::true_type{};

template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any &a) {}
template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any const &a) {}
template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any *a) {}
template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any const *a) {}

template < class Any1, class Any2 >
bool equal(Any1, Any2)
{}

#pragma endregion 

namespace alg
{

#pragma region BELT
    struct belt
    {
        typedef std::uint32_t key_t[8];
        typedef std::uint32_t block_t[4];
        typedef block_t iv_t;
        typedef std::uint64_t mac_t;

        //block cipher low-level traits
        void e(key_t k[1], block_t x[1]);
        void d(key_t k[1], block_t x[1]);

        //mac
        void gmac(block_t s[1], block_t x[1]);
        void omac(block_t s[1], block_t x[1]);

        //hash
        typedef block_t hash_val_t[2];
        void h(block_t z[3], block_t x[2]);
        void h_last(block_t z[5], block_t x[2], size_t bits);
    };

    //some types
    //e(k;x)=y; k(alg;usage;tag)
    template < class Key, class Something >
    struct encrypted {};
#pragma endregion

#pragma region BIGN

    struct bign{};
    //namespace bign{

    //template < size_t l >
    //struct bign
    //{
    //    static_assert(128 == l || 192 == l || 256 == l, "level 'l' must be 128 or 192 or 256.");

    //    static size_t const n = 2 * 32 * l / 128;
    //    typedef std::uint32_t pri_t[n];
    //    typedef std::uint32_t otk_t[n];
    //    typedef struct
    //    {
    //        std::uint32_t x[n];
    //        std::uint32_t y[n];
    //    } pub_t;
    //    typedef struct
    //    {
    //        size_t sz; // 1 < sz < 256
    //        std::uint8_t o[1]; // o[sz];
    //    } oid_t;
    //    typedef std::uint32_t hash_val_t[n];
    //    typedef std::uint32_t sig_t[n * 3 / 2];

    //    void sign_otk(pri_t pri[1], otk_t otk[1], hash_val_t h[1], oid_t o[1], sig_t s[1]);
    //    template < class Rng >
    //    void sign_rng(pri_t pri[1], Rng rng, hash_val_t h[1], oid_t o[1], sig_t s[1]);
    //    template < class Rng >
    //    void sign_orng(pri_t pri[1], Rng rng, hash_val_t h[1], oid_t o[1], sig_t s[1]);

    //    void verify(pub_t pub[1], hash_val_t h[1], oid_t o[1], sig_t s[1]);

    //    template < class Rng >
    //    void wrap_token(pub_t pub[1], Rng rng, std::uint8_t const *key, size_t key_size, std::uint8_t *token);

    //    static size_t const header_size = 16;
    //    static size_t const tpub_size = n / 2;
    //    //constexpr size_t calc_token_size(size_t key_size)
    //    //{
    //    //    return key_size + header_size + tpub_size;
    //    //}
    //    void unwrap_token(pri_t pri[1], std::uint8_t const *token, size_t token_size, std::uint8_t *key);
    //};


    //class bign //: public asymmetric_keys::asymmetric_key
    //{
    //};

    //class bign_private_key //: public ikey, bign
    //{
    //public:
    //    bign_signature sign();
    //};
    //class bign_public_key //: public ikey, bign
    //{
    //public:
    //    bool verify(bign_signature &s);
    //};

    //class bign_signature
    //{
    //};


#pragma endregion

#pragma region ANY_ALG
    struct any_alg{};
#pragma endregion

}

namespace type_functions {
    template <class T>
    struct is_key : std::false_type {};

    template <class Key>
    struct s_key_alg {};
    template <class Key>
    using key_alg = typename std::enable_if<is_key<Key>::value
        , typename s_key_alg<Key>::type>::type;

    template <class Key>
    struct s_key_usage {};
    template <class Key>
    using key_usage = typename s_key_usage<Key>::type;

}

namespace keys
{

    template < class Alg, template < class > class KeyRep >
    class symmetric_key
    {
    public:
        void e();
        void ke();

        void d();
        void kd();
    };
    template < class Alg, template < class > class KeyRep >
    class asymmetric_key
    {
    public:
    };

    //template < class Rep>
    class belt_key : public alg::belt
    {
    public:
        belt_key()
        {
            generate_key();
        }
        //belt_key(Rep rep)
        //{
        //}

        //belt_key(belt_key const &) = default;
        //belt_key(belt_key &&) = default;
        //belt_key &operator=(belt_key const &) = default;
        //belt_key &operator=(belt_key &&) = default;


        //??
        //template<>
        void generate_key()
        {
        }
        //template<>
        void replicate()
        {
        }


        //template< class Plain, class Cipher >
        //encrypted_data e(Plain const &plain, Cipher &cipher)
        //{
        //    return encrypted_data();
        //}
        //template< class Plain, class Cipher >
        //Cipher::type e(Plain const &plain, Cipher &cipher)
        //{
        //    
        //}

        //    template < class Plain, class Cipher >
        //    typename std::enable_if< true
        //        && std::is_same< get_rawrep<Plain>, block_t >::value
        //        && std::is_same< get_rawrep<Cipher>, block_t >::value
        //        && std::is_same< get_tag<Cipher>, Encrypted<get_tag<Plain> > >::value
        //    >::type e(Plain const &plain, Cipher &cipher)
        //    {
        //        auto x = get_key_bytes(plain);
        //        assign(cipher, plain);
        //        Alg::e(get_ptr(*this), get_ptr(cipher));
        //    }  

        template < class Plain, class Cipher >
        void e(Plain const &plain, Cipher &cipher)
        {
            //TODO  
        }

        template< class Plain, class Cipher >
        void ke(Plain const &plain, Cipher &cipher)
        {
            //TODO
        }


        template < class Plain, class Cipher >
        void d(Cipher const &cipher, Plain &Plain)
        {
            //TODO
        }

        //template < class Cipher, class Plain >
        //typename std::enable_if< is_key<Cipher>::value >::type
        //    kd(Cipher const &cipher, Plain &Plain);

    };

    class bign_key : public alg::bign
    {
    public:
        bign_key();
    };


    //template < class Alg, template < class > class KeyRep >
    //typename std::enable_if< std::is_same<Alg, alg::belt::belt_key>, alg::belt::key >::type ikey;
    template < class Alg, template < class > class KeyRep >
    using ikey = typename std::enable_if <
        std::is_same<Alg, alg::belt> ::value
        || std::is_same<Alg, alg::bign> ::value,
        //|| std::is_same<Alg, alg::any_alg> ::value,
        belt_key
    >::type;


    namespace symmetic_keys
    {

        typedef std::uint32_t key_t[8];
        typedef std::uint32_t block_t[4];
        typedef block_t iv_t;
        typedef std::uint64_t mac_t;

        enum emode
        {
            ecb,
            ecb_cts,
            cbc,
            cbc_cts,
            cfb,
            ctr,
            ofb,
        };
        enum amode
        {
            omac,
            gmac,
        };
        enum aemode
        {
            gcm,
        };
        //some abstraction level

        namespace block {

            template < class Alg >
            class cipher : public Alg
            {
            public:
                void encrypt_next();
                void decrypt_next();
            };

            template < class Alg >
            class ecb_cts : public Alg
            {
            public:
                typedef typename Alg::key_t key_t;
                typedef typename Alg::block_t block_t;
                void encrypt_next();
                void encrypt_cts(key_t const k[1], block_t py[1], block_t y[1], block_t const x[1], size_t tail_bits)
                {
                }
            };

            template <class Alg>
            class hash
            {
                typedef int block_t;
                typedef int val_t;

                void init();
                void next(block_t const *x);
                void last(block_t const *x, size_t tail_bits);
                void done();
                void val(val_t *y);
                void clear();
                void clone(hash *h) const;
            };
            template < class Hash >
            class hmac
            {
                typedef typename Hash::block_t block_t;
                typedef typename Hash::val_t val_t;

                void init();
                void next(block_t const *x);
                void last(block_t const *x, size_t tail_bits);
                void done();
                void val(val_t *y);
                void clear();
                void clone(hmac<Hash> *h) const;
            };

        }
        namespace blocks
        {

            template < class BlockAlg >
            class blocks
            {
            public:
                template < class IBlocks, class OBlocks >
                void encrypt(IBlocks ibegin, IBlocks iend, OBlocks out, size_t tail_bits)
                {
                    block_t y[1];
                    for (; ibegin != iend; ++ibegin)
                    {
                        BlockAlg::e(*ibegin, y);
                        *out++ = *y;
                    }
                    BlockAlg::e_last(*ibegin, y, tail_bits);
                    *out++ = std::make_pair(*y, tail_bits);
                }
            };
        }
        namespace buffered
        {
            template < class BlocksAlg >
            class buffered
            {
                block_t _buf[1024];
                size_t _off, _sz;
            public:
                void encrypt(std::uint8_t const *x, std::uint8_t *y, size_t sz)
                {
                    sz = std::min(sz, sizeof(_buf));
                    memcpy(_buf, x, sz);
                    BlocksAlg::encrypt(_buf, _buf + _off);
                }
            };

        }
        namespace stream
        {

        }

    }
    namespace asymmetric_keys
    {
        //some abstraction level

    }
}

namespace todo
{
    class bels
    {
        //share secret key
        //recover secret key
    };
    class bake
    {
    };

}

void test()
{
    //belt

    //key is local, data is local, try encrypt
#if 0
    typedef std::vector< uint8_t> bytes;
    auto key_data = bytes(16, 16);
    auto data = bytes(32, 32);

    auto converted_data = any_pod_val< bytes >(data);
    auto converted_key_data = any_pod_val< bytes >(key_data);

    auto key = keys::ikey< alg::belt::belt, any_pod_val>(converted_key_data);

    auto encrypted_data = key.e(converted_data);
    auto decrypted_data = key.d(encrypted_data);

    if (decrypted_data.data() != converted_data.data())
    {

    }
#endif
    {
        auto key = keys::ikey< alg::belt, any_pod_val>();
        auto plain = any_data();
        auto cipher = any_data();
        //auto plain = any_pod_val< std::vector< uint8_t>>();
        //auto cipher = any_pod_val< std::vector< uint8_t>>();

        
        key.e(plain, cipher);
        key.d(cipher, plain);
        //assert();
    }
    {
        auto key = keys::ikey< alg::belt, any_pod_val >();


    }


}


int main()
{
    test();
}
