//#include <type_traits>
//#include <vector>
//#include <array>
//#include <string>
//#include <iostream>
//#include <tuple>
//#include <xmmintrin.h>
//#include <cstdint>
//#include <cassert>
//#include <random>
//
//namespace impl {
//
//    /*
//    - block-level functions (operate upon single block):
//    - typedefs provide block_t & keyx_t (keyx_t - is expanded key, actually used by algorithms);
//    - typedefs also provide iv_t, tag_t (authentication tag), key_t<L>, hash_val_t;
//    - basic belt functions are block encryption e(k,x) and block decryption d(k,x);
//    - hash double-block update function h(ctx,x[2]);
//    - authentication functions: omac(s,x) & gmac(s,x);
//    - blocks-level functions (operate upon complete chain of blocks):
//    - on top of block cryption functions cipher-modes are build: ecb, ecb_cts, cbc, cbc_cts, cfb, ctr; pcbc, ofb;
//    - on top of hash update function hbelt algorithm is built;
//    - on top of authentication functions mac algorithm & ae cipher-modes are build: omac, gcm; eax, xts, cwc;
//    - additional functions:
//    - key expansion;
//    - key derivation (key-rep);
//    - key-e, key-d - key encryption & decryption;
//    */
//    struct belt
//    {
//        typedef std::uint32_t key_t[8];
//        typedef std::uint32_t block_t[4];
//        typedef block_t iv_t;
//        typedef std::uint64_t mac_t;
//
//        //block cipher low-level traits
//        void e(key_t k[1], block_t x[1]);
//        void d(key_t k[1], block_t x[1]);
//
//        //mac
//        void gmac(block_t s[1], block_t x[1]);
//        void omac(block_t s[1], block_t x[1]);
//
//        //block cipher modes
//        //ecb, ecb_cts, cbc, cbc_cts, ofb, ctr
//        //gcm
//
//        //hash
//        typedef block_t hash_val_t[2];
//        void h(block_t z[3], block_t x[2]);
//    };
//
//    /*
//    - basic bign functions are:
//    - gen private keys/val public keys/val params;
//    - sign hash/verify signature;
//    - wrap/unwrap token;
//    - id-sign hash/id-verify signature;
//    */
//    template < size_t l >
//    struct bign
//    {
//        static_assert(128 == l || 192 == l || 256 == l, "level 'l' must be 128 or 192 or 256.");
//
//        static size_t const n = 2 * 32 * l / 128;
//        typedef std::uint32_t pri_t[n];
//        typedef std::uint32_t otk_t[n];
//        typedef struct
//        {
//            std::uint32_t x[n];
//            std::uint32_t y[n];
//        } pub_t;
//        typedef struct
//        {
//            size_t sz; // 1 < sz < 256
//            std::uint8_t o[1]; // o[sz];
//        } oid_t;
//        typedef std::uint32_t hash_val_t[n];
//        typedef std::uint32_t sig_t[n * 3 / 2];
//
//        void sign_otk(pri_t pri[1], otk_t otk[1], hash_val_t h[1], oid_t o[1], sig_t s[1]);
//        template < class Rng >
//        void sign_rng(pri_t pri[1], Rng rng, hash_val_t h[1], oid_t o[1], sig_t s[1]);
//        template < class Rng >
//        void sign_orng(pri_t pri[1], Rng rng, hash_val_t h[1], oid_t o[1], sig_t s[1]);
//
//        void verify(pub_t pub[1], hash_val_t h[1], oid_t o[1], sig_t s[1]);
//
//        template < class Rng >
//        void wrap_token(pub_t pub[1], Rng rng, std::uint8_t const *key, size_t key_size, std::uint8_t *token);
//
//        static size_t const header_size = 16;
//        static size_t const tpub_size = n / 2;
//        //constexpr size_t calc_token_size(size_t key_size)
//        //{
//        //    return key_size + header_size + tpub_size;
//        //}
//        void unwrap_token(pri_t pri[1], std::uint8_t const *token, size_t token_size, std::uint8_t *key);
//    };
//
//    struct test_pod
//    {
//        typedef std::uint32_t block_t;
//        typedef std::uint64_t key_t;
//
//        static void e(key_t const *k, block_t *x)
//        {
//            *x = (*x ^ static_cast< block_t >(*k)) + static_cast< block_t >(*k >> 32);
//        }
//        static void d(key_t const *k, block_t *x)
//        {
//            *x = (*x - static_cast< block_t >(*k >> 32)) ^ static_cast< block_t >(*k);
//        }
//    };
//    //!!
//    struct test_carray
//    {
//        typedef std::uint32_t block_t[2];
//        typedef std::uint32_t key_t[4];
//
//        //typedef block_t key_t[2];
//        static void e(key_t const *k, block_t *x)
//        {
//            (*x)[0] = ((*x)[0] ^ (*k)[0]) + (*k)[2];
//            (*x)[1] = ((*x)[1] ^ (*k)[1]) + (*k)[3];
//        }
//        static void e(std::uint32_t const *rk, std::uint32_t *rx)//std::uint32_t *rx)
//        {
//            key_t const *k = reinterpret_cast< key_t const * >(rk);
//            block_t *x = reinterpret_cast< block_t * >(rx);
//            e(k, x);
//        }
//        static void e(key_t const *k, std::uint32_t *rx)//std::uint32_t *rx)
//        {
//            block_t *x = reinterpret_cast< block_t * >(rx);
//            e(k, x);
//        }
//        static void e(std::uint32_t const *rk, block_t *x)//std::uint32_t *rx)
//        {
//            key_t const *k = reinterpret_cast< key_t const * >(rk);
//            e(k, x);
//        }
//        static void d(key_t const *k, block_t *x)
//        {
//            (*x)[0] = ((*x)[0] - (*k)[2]) ^ (*k)[0];
//            (*x)[1] = ((*x)[1] - (*k)[3]) ^ (*k)[1];
//        }
//        static void d(std::uint32_t const *rk, std::uint32_t *rx)
//        {
//            key_t const *k = reinterpret_cast< key_t const * >(rk);
//            block_t *x = reinterpret_cast< block_t * >(rx);
//            d(k, x);
//        }
//        static void d(key_t const *k, std::uint32_t *rx)
//        {
//            block_t *x = reinterpret_cast< block_t * >(rx);
//            d(k, x);
//        }
//        static void d(std::uint32_t const *rk, block_t *x)
//        {
//            key_t const *k = reinterpret_cast< key_t const * >(rk);
//            d(k, x);
//        }
//
//    };
//
//    struct test_struct
//    {
//        typedef struct s_block
//        {
//            std::uint32_t a, b;
//        } block_t;
//        typedef struct
//        {
//            block_t u, v;
//        } key_t;
//        static void e(key_t const *k, block_t *x)
//        {
//            (*x).a = ((*x).a ^ (*k).u.a) + (*k).v.a;
//            (*x).b = ((*x).b ^ (*k).u.b) + (*k).v.b;
//        }
//        static void d(key_t const *k, block_t *x)
//        {
//            (*x).a = ((*x).a - (*k).v.a) ^ (*k).u.a;
//            (*x).b = ((*x).b - (*k).v.b) ^ (*k).u.b;
//        }
//    };
//}
//
//#pragma region ANY_POD
//template < class Rep >
//class any_pod_val
//{
//public:
//    typedef Rep rep_t;
//    //static_assert(std::is_copy_constructible<Rep>::value, "Rep type must be copy-constructible.");
//    //any_pod_val(rep_t const &value) : _value(value) {}
//    static_assert(true
//        && !std::is_reference<Rep>::value
//        && std::is_pod<Rep>::value
//        , "Rep type must be pod.");
//    any_pod_val(rep_t const &value) { memcpy(&_value, &value, sizeof(_value)); }
//
//    any_pod_val() = default;
//    any_pod_val(any_pod_val const &) = default;
//    any_pod_val(any_pod_val &&) = default;
//    any_pod_val &operator=(any_pod_val const &) = default;
//    any_pod_val &operator=(any_pod_val &&) = default;
//
//    template < class T >
//    any_pod_val(T const &a
//        , typename std::enable_if< !std::is_same<any_pod_val, T>::value && is_any<T>::value >::type ** = nullptr
//        ) : base()
//    {
//        memcpy(&_value, a.p(), sizeof(_value));
//    }
//
//    template < class T >
//    typename std::enable_if< !std::is_same<any_pod_val, T>::value && is_any<T>::value
//        , any_pod_val & >::type operator=(T const &a)
//    {
//        memcpy(&_value, a.p(), sizeof(_value));
//        return *this;
//    }
//
//    rep_t *p() { return &_value; }
//    rep_t const *p() const { return &_value; }
//    //private:
//    rep_t _value;
//};
//template < class Rep >
//bool operator==(any_pod_val<Rep> const &l, any_pod_val<Rep> const &r)
//{
//    static_assert(std::is_pod<Rep>::value, "Cannot memcmp Rep type.");
//    return 0 == memcmp(l.p(), r.p(), sizeof(Rep));
//    //return (*l.p()) == (*r.p());
//    return false;
//}
//template < class Rep >
//bool operator!=(any_pod_val<Rep> const &l, any_pod_val<Rep> const &r)
//{
//    return !(l == r);
//}
//
//template < class Rep > struct is_any< any_pod_val<Rep> > : std::true_type{};
//
//template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any &a) {}
//template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any const &a) {}
//template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any *a) {}
//template < class Any > typename std::enable_if< std::is_pod<Any>::value, Any *>::type get_p_from_any(Any const *a) {}
//
//template < class Any1, class Any2 >
//bool equal(Any1, Any2)
//{}
//
//
//template < class Rep >
//class any_pod_ref
//{
//public:
//    typedef Rep rep_t;
//    //static_assert(std::is_copy_constructible<Rep>::value, "Rep type must be copy-constructible.");
//    //any_pod_ref(rep_t const &value) : _value(value) {}
//    static_assert(true
//        && !std::is_reference<Rep>::value
//        && std::is_pod<Rep>::value
//        , "Rep type must be pod.");
//    //&& std::is_reference<Rep>::value
//    //&& std::is_pod<typename std::remove_reference<Rep>::type>::value
//    //, "Rep type must be pod reference.");
//    any_pod_ref(rep_t &value) : _value(value) {}
//
//    any_pod_ref() = default;
//    any_pod_ref(any_pod_ref const &) = default;
//    any_pod_ref(any_pod_ref &&) = default;
//    any_pod_ref &operator=(any_pod_ref const &) = default;
//    any_pod_ref &operator=(any_pod_ref &&) = default;
//
//    template < class T >
//    any_pod_ref(T const &a
//        , typename std::enable_if< !std::is_same<any_pod_ref, T>::value && is_any<T>::value >::type ** = nullptr
//        ) : _value(*a.p())
//    {
//    }
//
//    template < class T >
//    typename std::enable_if< !std::is_same<any_pod_ref, T>::value && is_any<T>::value
//        , any_pod_ref & >::type operator=(T const &a)
//    {
//        memcpy(&_value, a.p(), sizeof(_value));
//        return *this;
//    }
//
//    rep_t *p() { return &_value; }
//    rep_t const *p() const { return &_value; }
//    //private:
//    rep_t &_value;
//};
//template < class Rep >
//bool operator==(any_pod_ref<Rep> const &l, any_pod_ref<Rep> const &r)
//{
//    static_assert(std::is_pod<Rep>::value, "Cannot memcmp Rep type.");
//    return 0 == memcmp(l.p(), r.p(), sizeof(Rep));
//    //return (*l.p()) == (*r.p());
//    return false;
//}
//template < class Rep >
//bool operator!=(any_pod_ref<Rep> const &l, any_pod_ref<Rep> const &r)
//{
//    return !(l == r);
//}
//
//template < class Rep > struct is_any< any_pod_ref<Rep> > : std::true_type{};
//
//
//#pragma endregion
//
//
//template < class Alg, class KeyTag, class PlainTag >
//struct Encrypted {};
//
//template < class Alg, template < class > class KeyRep /*= any_pod_val*/ >
//class key : public KeyRep< typename Alg::key_t >
//{
//    typedef KeyRep< typename Alg::key_t > base;
//    typedef typename Alg::key_t key_t;
//    typedef typename Alg::block_t block_t;
//public:
//    typedef typename base::rep_t rep_t;
//    static_assert(std::is_pod<key_t>::value, "key_t type must be pod.");
//
//    //key(rep_t const &key) : base(key) {}
//    template < class ...Args >
//    key(Args &&...args) : base(std::forward<Args>(args)...){}
//
//    template < class Arg >
//    key &operator=(Arg &&arg)
//    {
//        static_cast<base &>(*this) = std::forward<Arg>(arg);
//        return *this;
//    }
//
//    template < class Plain, class Cipher >
//    typename std::enable_if< true
//        && std::is_same< get_rawrep<Plain>, block_t >::value
//        && std::is_same< get_rawrep<Cipher>, block_t >::value
//        && std::is_same< get_tag<Cipher>, Encrypted<get_tag<Plain> > >::value
//    >::type e(Plain const &plain, Cipher &cipher)
//    {
//        auto x = get_key_bytes(plain);
//        assign(cipher, plain);
//        Alg::e(get_ptr(*this), get_ptr(cipher));
//    }  
//
//    template < class Cipher, class Plain >
//    typename std::enable_if< true
//        && std::is_same< get_rawrep<Cipher>, block_t >::value
//        && std::is_same< get_rawrep<Plain>, block_t >::value
//        && std::is_same< get_tag<Plain>, Encrypted<get_tag<Cipher> > >::value
//    >::type d(Cipher const &cipher, Plain &Plain)
//    {
//        auto x = get_key_bytes(plain);
//        assign(plain, cipher);
//        Alg::e(get_ptr(*this), get_ptr(plain));
//    }
//
//};
//
//
//// specific application-level type information
//namespace tag {
//    template < class Alg, class KeyTag, class PlainTag >
//    struct encrypted_block {};
//
//    namespace default {
//        struct plain_data {};
//        struct data_encryption_key {};
//        struct key_encryption_key {};
//        struct key_protection_password {};
//    }
//}
//
//template <class Rep>
//class any
//{
//public:
//    typedef Rep rep_t;
//    static_assert( true , "Rep type must be or not be.");
//
//    any() = default;
//    any(any const &) = default;
//    any(any &&) = default;
//    any &operator=(any const &) = default;
//    any &operator=(any &&) = default;
//
//    
//    rep_t *p() 
//    { 
//        return &_value; 
//    }
//    rep_t const *p() const 
//    {
//        return &_value; 
//    }
//
//private:
//    rep_t _value;
//};
//
//
//void test()
//{
//    //auto data = std::vector< uint8_t>();
//    //plain
//    //cipher
//
//    //auto k = key< impl::belt, any_pod_val >();
//
//    auto vt = std::vector<uint8_t>();
//    auto at = std::array< uint8_t, 2 >();
//    
//    //auto avt = any< std::vector<uint8_t> >(vt);
//    //auto avt_ref = any< std::vector<uint8_t> >(&vt);
//
//
//}
//
////int _tmain(int argc, _TCHAR* argv[])
////{
////
////    test();
////    return 0;
////}
//
//
//
//
