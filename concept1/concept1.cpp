#include <type_traits>
#include <vector>
#include <array>
#include <string>
#include <iostream>
#include <tuple>
#include <xmmintrin.h>
#include <cstdint>
#include <cassert>
#include <random>
#include <memory>

//#include <cdk/>
//#include <boost/>

typedef std::vector<uint8_t> bytes;


template <class ...Fs> // fm(.. f2(f1(x1,..,xn))..)
struct compose
{};
template <class ...Fs> // (Fs(xs...)...) = (f1(x1,..,xn), .., fm(x1,..,xn))
struct split
{
    std::tuple<Fs...> fs;
    template <class ...Us>
    split(Us &&...us) : fs(std::forward<Us>(us)...) {}

    template <size_t i, class ...Args>
    typename std::enable_if< i < sizeof...(Fs) >::type
        apply(Args &&...args)
    {
        std::get<i>(fs) (std::forward<Args>(args)...);
        apply<i + 1>(std::forward<Args>(args)...);
    }
    template <size_t i, class ...Args>
    typename std::enable_if< !(i < sizeof...(Fs)) >::type
        apply(Args &&...args)
    {
    }

    //template <class ...Args>
    //auto operator()(Args &&...args)
    //{
    //    return apply<0>(std::forward<Args>(args)...);
    //}

};

template <class ...Args>
struct concat {};



#if 0
struct protector
{
    auto token;
    void init(auto rng, auto receiver_pub)
    {
        auto (tpub, kek) = secret_agreement(rng, receiver_pub);
        auto key_crypter = make_key_crypter(kek);
        auto dek = gen_key(rng);
        auto edek = key_crypter(dek + dek_header);
        token = dek_header + tpub + edek;

        auto hasher = make_hasher();
        auto aad = token;
        auto crypter = make_crypter<gcm>(dek, rng);
    }
    auto operator()(auto data)
    {
        auto iv_edata_tag = crypter(data, aad);
        auto hash_value = hasher(data);
    }
    void done(auto sender_pri, auto token_pwd)
    {
        auto signer = make_signer(sender_pri, rng);
        auto sign = signer(hash_value);

        auto tek = key_from_password(token_pwd);
        auto token_crypter = make_key_crypter(tek);
        auto etoken = token_crypter(token);

        auto blob = etoken + iv_edata_tag + sign;
        auto blob64 = base64(blob);
        return blob64;
    }
};
#endif

//
//template <class Rep = bytes>
//class assymetric_key
//{
//public:
//Rep generate_key_pair()
//{
//
//};
//Rep generate_key_pair(random &rng)
//{
//return Rep();
//};
//Rep generate_key_pair(Rep &rng)
//{
//return Rep();
//};
//
//};
//

class public_key
{
    //bign_std128_pub_t rep;
    //private_key() {}
public:

};

#pragma region RNG
class rng_concept
{
public:
    template <class T>
    std::enable_if_t<std::is_standard_layout<T>::value, T> operator()() { return std::declval<T>(); }
    template <class T>
    T &operator()(T &seed) { return seed; }
};

template <class Engine>
class std_rng
{
public:
    Engine e;

    std_rng(){};
    ~std_rng(){};

    template <class T>
    std::enable_if_t<std::is_standard_layout<T>::value, T> operator()()
    {
        //union
        //{
        //    T rnd;
        //    std::uint8_t rnd_bytes[sizeof(T)];
        //};

        T rnd;
        auto &rnd_bytes = *reinterpret_cast< std::uint8_t(*)[sizeof(T)] >(&rnd);

        std::generate(std::begin(rnd_bytes), std::end(rnd_bytes), std::ref(e));
        return rnd;
    }
    template <class T>
    T &operator()(T &seed) { return seed; }

    std_rng(std_rng& other);


};

typedef std_rng<std::random_device> system_rng;
typedef std_rng<std::default_random_engine> default_rng;

system_rng get_system_rng() { return system_rng(); }
default_rng make_default_rng(int seed = 7) { auto rng = default_rng(); rng.e.seed(seed); return rng; }
#pragma endregion

#pragma region PRI

class private_key
{
    //bign_std128_pri_t rep;

    private_key() {}
    private_key(private_key const &);
    private_key(private_key &&);
    private_key &operator=(private_key const &);
    private_key &operator=(private_key &&);
    template <class ...Args>
    private_key(Args &&...args);
public:
    template <class Rng>
    static private_key gen(Rng const &rng)
    {
        private_key pri;
        rng(pri.rep);
        return pri;
    }
    public_key calc_pub() const
    {
    }

    
    template< class Rep = bytes>
    Rep calc_pub()
    {
        return bign_pub();
    }
    template< class Rep = bytes>
    bytes calc_pub(bytes &pri)
    {
        return bytes(16, 16);
    }
};
template <class Rng>
private_key gen_pri(Rng &rng)
{
    return private_key::gen(rng);
}


#pragma endregion

#if 0
bytes gen_pri(bytes &rng)
{
    return rng;
};
bytes calc_pub(bytes &pri)
{
    return bytes(16, 16);
};
bytes read_file(std::string &name)
{
    return bytes(16, 16);
}


bytes tpub_from_pub(bytes &tpri)
{
    return tpri;
};

bytes tdh(bytes &tpri, bytes &tpub)
{
    return bytes(16, 16);
}
bytes make_key_crypter(bytes &kek)
{

};
bytes key_crypter(bytes &dek)
{
    return bytes(16, 16);
};
bytes gen_key(bytes/*random*/ &rng)
{
    return rng;
};

class hasher;
hasher make_hasher();

template <class Hash, class DataTag>
class hash_value {};

template <class Hash>// = block::hash<alg::belt>
class hasher
{
private:
    friend hasher make_hasher();
    hasher() { init(); }
    ~hasher() { clear(); }
public:
    template <class Data>
    hash_value<Hash, get_tag<Data>> operator()(bytes const &data)
    {
        for (;;)
            next();
        last();
        done();
        value();
    }

};

hasher make_hasher()
{
    return hasher();
}


//template<CipherMode>
bytes make_crypter()
{

    return bytes(16, 16);
};

bytes make_signer(bytes &sender_pri, bytes/*random*/ &rng)
{
    return bytes(16, 16);
};
bytes signer(bytes &hash_value)
{
    return bytes(16, 16);
};
bytes key_from_password(std::string &pwd)
{
    return bytes(16, 16);
};

bytes token_crypter(bytes &token)
{
    return bytes(16, 16);
};

//key<Alg, Usage, Tag>
//concat<iv, encrypted<Key,Data>, mac<Key, encrypted<Key,Data>>>
//crypter(bytes &data, bytes &aad)
//{
//
//};
//template<class public_key>
std::pair< bytes, bytes>
secret_agreement(bytes/*random*/ &rng, bytes/*public_key*/ &receiver_pub)
{
    // y^2 = x^3 + ax + b = f(x)
    // G = (G_x,G_y); G_x = 0; G_y = +- sqrt(b)
    // P=(x,y); (x,-y) = -P

    // dQ = (dQ_x, dQ_y)
    // d(-Q) = - dQ = (dQ_x, -dQ_y)
    auto tpri = gen_pri(rng); // d
    auto tpub = tpub_from_pub(calc_pub(tpri)); // dG
    //receiver_pub = yield(tpub);

    // eG = Q
    auto rtpub = tpub_from_pub(receiver_pub); // (eG_x); eG_y = +- sqrt( f(eG_x) )
    // +- Q
    auto shared = tdh(tpri, rtpub); // d(eG) = (x'',y'')
    // tdh() = deG_x

    //yield(shared);
    return std::make_pair(tpub, shared);
};



//bytes protectt(bytes/*random*/ &rng, auto sender_pri, auto receiver_pub, auto token_pwd, auto data)
bytes protect(bytes/*random*/ &rng, bytes &sender_pri, bytes &receiver_pub, bytes &token_pwd, bytes &data)
{
    auto tpub_kek/*(tpub, kek)*/ = secret_agreement(rng, receiver_pub);
    auto key_crypter = make_key_crypter(tpub_kek.second); //kek
    auto dek = gen_key(rng);

    bytes dek_header;
    auto edek = make_crypter(dek + dek_header); //dek + dek_header ????
    auto token = dek_header + tpub_kek.first + edek; //dek_header + tpub + edek;

    auto hasher = make_hasher();
    //auto hash_value = hasher(data);
    auto aad = token;
    auto crypter = make_crypter(dek, rng); //make_crypter<gcm>(dek, rng);

    //auto f = make_split(std::ref(crypter), std::ref(hasher));
    //data >> f >> iv_edata_tag;
    //stream(data, f, output);
    //stream(aad, crypter, nooutput);
    ////fmap(crypter, data);
    ////fmap(hasher, data);
    ////fmap(signed_data_encoder, data);
    ////fmap(signed_data_decoder, data);
    //auto iv_edata_tag = crypter();
    //auto hash_value = hasher();

    //key<Alg, Usage, Tag>
    //concat<iv, encrypted<Key,Data>, mac<Key, encrypted<Key,Data>>>
    auto iv_edata_tag = crypter(data, aad);
    //hash<Alg>
    //hash_value<Hash, Data>
    auto hash_value = hasher(data);

    //pri<Alg, Usage, Tag>
    auto signer = make_signer(sender_pri, rng);
    //signature<Pri, HashValue>
    auto sign = signer(hash_value);

    //password<Tag>
    auto tek = key_from_password(token_pwd);
    auto token_crypter = make_key_crypter(tek);
    auto etoken = token_crypter(token);

    //make_token(rng, receiver_pub)
    //compose(prepender(etoken), appender(sign));

    auto blob = etoken + iv_edata_tag + sign;
    return blob; //blob64
}
//auto unprotect(auto receiver_pri, auto sender_pub, auto token_pwd, auto blob)
//{
//
//    auto (tpub, kek) = secret_agreement(rng, receiver_pub);
//    auto key_crypter = make_key_crypter(kek);
//    auto dek = gen_key(rng);
//    auto edek = key_crypter(dek + dek_header);
//    auto token = dek_header + tpub + edek;
//
//    auto hasher = make_hasher();
//    auto aad = token;
//    auto crypter = make_crypter<gcm>(dek, rng);
//
//    //auto f = make_split(std::ref(crypter), std::ref(hasher));
//    //data >> f >> iv_edata_tag;
//    //stream(data, f, output);
//    //stream(aad, crypter, nooutput);
//    ////fmap(crypter, data);
//    ////fmap(hasher, data);
//    ////fmap(signed_data_encoder, data);
//    ////fmap(signed_data_decoder, data);
//    //auto iv_edata_tag = crypter();
//    //auto hash_value = hasher();
//
//    //key<Alg, Usage, Tag>
//    //concat<iv, encrypted<Key,Data>, mac<Key, encrypted<Key,Data>>>
//    auto iv_edata_tag = crypter(data, aad);
//    //hash<Alg>
//    //hash_value<Hash, Data>
//    auto hash_value = hasher(data);
//
//    //pri<Alg, Usage, Tag>
//    auto signer = make_signer(sender_pri, rng);
//    //signature<Pri, HashValue>
//    auto sign = signer(hash_value);
//
//    //password<Tag>
//    auto tek = key_from_password(token_pwd);
//    auto token_crypter = make_key_crypter(tek);
//    auto etoken = token_crypter(token);
//
//    //make_token(rng, receiver_pub)
//    //compose(prepender(etoken), appender(sign));
//
//    auto blob = etoken + iv_edata_tag + sign;
//    auto blob64 = base64(blob);
//    return blob64;
//
//}
#endif

void test()
{
    auto rng = make_default_rng(); //select_dkey_and_get_rng();

    //auto receiver_pri = gen_pri<std::uint8_t>( rng );
#if 0
    auto receiver_pub = calc_pub(receiver_pri);

    auto sender_pri = gen_pri(rng_receiver);
    auto sender_pub = calc_pub(sender_pri);

    auto receiver_pub = read_file(std::string("rpub.txt"));
    auto token_pwd = "pwd";

    auto data = read_file(std::string("data.txt"));
    //auto sink = base64_file_writer(file_name);
    //sink(data);

    //auto prepender = make_prepender(smth)
    //prepender(data) // smth + data
    //auto appender = make_appender(smth)
    //appender(data) // data + smth

    auto protectedd = protect(rng_receiver, sender_pri, receiver_pub, token_pwd, data);

    //auto protector = make_protector(rng, sender_pri, receiver_pub, token_pwd);
    //auto output = protector(data);

    //source1, source2
    //f1, f2, f3
    //sink

    //source = std::ifstream | std::cin | std::vector
    //sink = std::ofstream | std::cout | std::vector
    //arr = crypter | hasher
    //aecrypter = prepend(iv) + ecrypt(data) + acrypt(edata,aad) + append(tag)
    //combinators = compose (f,g) | parallel (f,g)
    //adcrypter = 
    //adcrypter2 = 

    //x = source + f1 + f2 + f3
    //x > file
    //x > stdout

    //stream(data, protector, output);

    //auto error_or_unprotected = unprotect(receiver_pri, sender_pub, token_pwd, blob);
    //assert(data == get_right(error_or_unprotected));
#endif
}

int main()
{
    test();
}

#ifdef TODO
test
- �������� unprotect;
-������ ���� �� ��� ��������� ������� + ������������ ��������;
-�������� / ��������� ������������� ��������� ���������� ����� auto;
-�������� ��� ���� ������� / �������;

-��������� ������ �� ������� ������ � ������ ����������;
-����������������
- ����:
-������� ���� �������� ����� �������,
-������� ���� ����������� ������� ����� �������;
-���������:
-������� � �������� ������� �� ����(enable_if);
-const, &, &&;
-����������:
-���������� belt / bign �� cdk;
���������;

test2 - hmac + brng + bels
- �� �������� � test �������� ������� �������� �������������
hmac, brng, bels;

test3 - stream
- ��������� �������� ������������� �� �������� �������
source, sink, arr, combinator;

#endif
